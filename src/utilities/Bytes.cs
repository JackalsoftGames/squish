﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Utilities
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains methods that read or write byte data
    /// </summary>
    /// <remarks>To be used as a compliment to System.BitConverter</remarks>
    public static class Bytes
    {
        #region Bytes.Read

        // IWriteable
        public static int Read<T>(byte[] buffer, int index, out T result)
            where T : IWriteable, new()
        {
            result = new T();
            return result.FromBytes(buffer, index);
        }

        // Unsigned
        public static int Read(byte[] buffer, int index, out Byte value)
        {
            value = buffer[index];
            return 1;
        }
        public static int Read(byte[] buffer, int index, out UInt16 value)
        {
            value = BitConverter.ToUInt16(buffer, index);
            return 2;
        }
        public static int Read(byte[] buffer, int index, out UInt32 value)
        {
            value = BitConverter.ToUInt32(buffer, index);
            return 4;
        }
        public static int Read(byte[] buffer, int index, out UInt64 value)
        {
            value = BitConverter.ToUInt64(buffer, index);
            return 8;
        }

        // Signed
        public static int Read(byte[] buffer, int index, out SByte value)
        {
            value = (SByte)buffer[index];
            return 1;
        }
        public static int Read(byte[] buffer, int index, out Int16 value)
        {
            value = BitConverter.ToInt16(buffer, index);
            return 2;
        }
        public static int Read(byte[] buffer, int index, out Int32 value)
        {
            value = BitConverter.ToInt32(buffer, index);
            return 4;
        }
        public static int Read(byte[] buffer, int index, out Int64 value)
        {
            value = BitConverter.ToInt64(buffer, index);
            return 8;
        }

        // Floating Point
        public static int Read(byte[] buffer, int index, out Single value)
        {
            value = BitConverter.ToSingle(buffer, index);
            return 4;
        }
        public static int Read(byte[] buffer, int index, out Double value)
        {
            value = BitConverter.ToDouble(buffer, index);
            return 8;
        }

        // Text/Other
        public static int Read(byte[] buffer, int index, out Boolean value)
        {
            value = BitConverter.ToBoolean(buffer, index);
            return 1;
        }
        public static int Read(byte[] buffer, int index, out Char value)
        {
            value = BitConverter.ToChar(buffer, index);
            return 1;
        }
        public static int Read(byte[] buffer, int index, out String value, int length)
        {
            value = System.Text.Encoding.Default.GetString(buffer, index, length);
            return length;
        }

        #endregion
        #region Bytes.Read[]

        // IWriteable
        public static int Read<T>(byte[] buffer, int index, out T[] result, int count)
            where T : IWriteable, new()
        {
            result = new T[count];

            int n = index;
            for (int i = 0; i < count; i++)
            {
                result[i] = new T();
                index += result[i].FromBytes(buffer, index);
            }

            return (index - n);
        }
        public static int Read<T>(byte[] buffer, int index, out IList<T> result, int count)
            where T : IWriteable, new()
        {
            result = new List<T>();

            int n = index;
            for (int i = 0; i < count; i++)
            {
                T item = new T();
                index += item.FromBytes(buffer, index);
                result.Add(item);
            }

            return (index - n);
        }

        // Unsigned
        public static int Read(byte[] buffer, int index, out Byte[] values, int count)
        {
            values = new byte[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out UInt16[] values, int count)
        {
            values = new UInt16[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out UInt32[] values, int count)
        {
            values = new UInt32[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out UInt64[] values, int count)
        {
            values = new UInt64[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }

        // Signed
        public static int Read(byte[] buffer, int index, out SByte[] values, int count)
        {
            values = new SByte[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out Int16[] values, int count)
        {
            values = new Int16[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out Int32[] values, int count)
        {
            values = new Int32[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out Int64[] values, int count)
        {
            values = new Int64[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }

        // Floating Point
        public static int Read(byte[] buffer, int index, out Single[] values, int count)
        {
            values = new Single[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out Double[] values, int count)
        {
            values = new Double[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }

        // Text/other
        public static int Read(byte[] buffer, int index, out Boolean[] values, int count)
        {
            values = new Boolean[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out Char[] values, int count)
        {
            values = new Char[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i]);

            return (index - n);
        }
        public static int Read(byte[] buffer, int index, out String[] values, int count, int length)
        {
            values = new String[count];

            int n = index;
            for (int i = 0; i < count; i++)
                index += Read(buffer, index, out values[i], length);

            return (index - n);
        }

        #endregion

        #region Bytes.Write

        // IWriteable
        public static int Write<T>(byte[] buffer, int index, T value)
            where T : IWriteable
        {
            return value.ToBytes(buffer, index);
        }

        // Unsigned
        public static int Write(byte[] buffer, int index, Byte value)
        {
            value = buffer[index];
            return 1;
        }
        public static int Write(byte[] buffer, int index, UInt16 value)
        {
            byte[] result = BitConverter.GetBytes(value);

            Array.Copy(result, 0, buffer, index, 2);
            return 2;
        }
        public static int Write(byte[] buffer, int index, UInt32 value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 4;
        }
        public static int Write(byte[] buffer, int index, UInt64 value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 8;
        }

        // Signed
        public static int Write(byte[] buffer, int index, SByte value)
        {
            buffer[index] = (byte)value;
            return 1;
        }
        public static int Write(byte[] buffer, int index, Int16 value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 2;
        }
        public static int Write(byte[] buffer, int index, Int32 value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 4;
        }
        public static int Write(byte[] buffer, int index, Int64 value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 8;
        }

        // Floating Point
        public static int Write(byte[] buffer, int index, Single value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 4;
        }
        public static int Write(byte[] buffer, int index, Double value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 8;
        }

        // Text/Other
        public static int Write(byte[] buffer, int index, Boolean value)
        {
            byte[] result = BitConverter.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return 1;
        }
        public static int Write(byte[] buffer, int index, Char value)
        {
            byte[] result = BitConverter.GetBytes(value);

            Array.Copy(result, 0, buffer, index, result.Length);
            return 1;
        }
        public static int Write(byte[] buffer, int index, String value)
        {
            byte[] result = System.Text.Encoding.Default.GetBytes(value);
            Array.Copy(result, 0, buffer, index, result.Length);

            return result.Length;
        }

        #endregion
        #region Bytes.Write[]

        // IWriteable
        public static int Write<T>(byte[] buffer, int index, T[] values)
            where T : IWriteable
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write<T>(byte[] buffer, int index, IList<T> values)
            where T : IWriteable
        {
            int n = index;
            for (int i = 0; i < values.Count; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }

        // Unsigned
        public static int Write(byte[] buffer, int index, Byte[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, UInt16[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, UInt32[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, UInt64[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }

        // Signed
        public static int Write(byte[] buffer, int index, SByte[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, Int16[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, Int32[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, Int64[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }

        // Floating Point
        public static int Write(byte[] buffer, int index, Single[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, Double[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }

        // Text/other
        public static int Write(byte[] buffer, int index, Boolean[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, Char[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }
        public static int Write(byte[] buffer, int index, String[] values)
        {
            int n = index;
            for (int i = 0; i < values.Length; i++)
                index += Write(buffer, index, values[i]);

            return (index - n);
        }

        #endregion
    }
}