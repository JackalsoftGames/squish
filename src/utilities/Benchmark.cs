﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Utilities
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains simple functions to help measure performance
    /// </summary>
    /// <remarks>To be used for debugging and optimizing functions with A/B testing</remarks>
    public static class Benchmark
    {
        /* This is used by wrapping the test you want in a function and supplying it to the action parameter.
         * It should not be used for formal speed tests, but as a quick-and-dirty way to see if one implementation
         * is significantly better/worse than another. Some factors like JIT inlining and compiler optimizations
         * for each platform should still be performed if necessary.
         * 
         * For example:
         * 
         * var r1 = Benchmark.RunTest(50, 1000, () =>
         * {
         *      float n = 0;
         *      for (int i = 0; i < 1000; i++)
         *      {
         *          n = Math.Sqrt(i);
         *      }
         * });
         * 
         * var r2 = Benchmark.RunTest(50, 1000, () =>
         * {
         *      float n = 0;
         *      for (int i = 0; i < 1000; i++)
         *      {
         *          n = Math.Power(i, 0.50d);
         *      }
         * });
         */

        public static long RunTest(int repeat, int count, Action action)
        {
            if (action == null || repeat <= 0 || count <= 0)
                return 0;

            var w = new System.Diagnostics.Stopwatch();
            long result = 0;

            action(); // invoke it once to get the JIT set up
            for (int j = 0; j < repeat; j++)
            {
                w.Restart();
                for (int i = 0; i < count; i++)
                    action();
                result += w.ElapsedTicks;
            }

            return (result / repeat);
        }
    }
}