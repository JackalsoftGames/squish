﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Utilities
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains a common implementation of the IDisposable interface
    /// </summary>
    /// <remarks>To be used to with any class with managed or unmanaged resources</remarks>
    public abstract class Disposable :
        IDisposable
    {
        // NOTE:
        // - This is roughly the same implementation that SFML.ObjectBase has
        // - These should not be used with SFML objects that derive from SFML.ObjectBase since
        //   SFML manages its own resources and this results in Dispose() being called twice.
        // https://archive.is/t9J9U :: https://stackoverflow.com/questions/538060/

        ~Disposable()
        {
            Dispose(false); // Ensures that unmanaged objects are always cleaned up (eventually)
        }

        public void Dispose()
        {
            if (m_canDispose)
            {
                Dispose(true);             // Call the client code to clean up managed and unmanaged resources
                GC.SuppressFinalize(this); // Tell the GC that we've done the work for it
                m_canDispose = false;      // Prevents dispose being called multiple times - client code doesn't need to worry
            }
        }
        
        private bool m_canDispose = true;
        protected virtual void Dispose(bool managed)
        {
            //(client code):  if true, dispose managed resources
            //if (managed)
            //{
            //    foreach (var ITEM in list) ITEM.Dispose();
            //    list.Clear();
            //}

            //(client code):  always dispose unmanaged resources
            //...
        }
    }
}