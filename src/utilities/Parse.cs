﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Utilities
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// A wrapper class around System.TryParse that contains methods to convert strings to primitive C# types or their default values
    /// </summary>
    /// <remarks>To be used to compliment System.TryParse</remarks>
    public static class Parse
    {
        public static Boolean AsBoolean(string value)
        {
            Boolean result;
            if (Boolean.TryParse(value, out result))
                return result;

            return default(Boolean);
        }
        public static Byte AsByte(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Byte result;
            if (Byte.TryParse(value, style, format, out result))
                return result;

            return default(Byte);
        }
        public static Byte AsByte(string value)
        {
            Byte result;
            if (Byte.TryParse(value, out result))
                return result;

            return default(Byte);
        }
        public static Char AsChar(string value)
        {
            Char result;
            if (Char.TryParse(value, out result))
                return result;

            return default(Char);
        }
        public static DateTime AsDateTime(string value, System.Globalization.DateTimeStyles style, IFormatProvider format)
        {
            DateTime result;
            if (DateTime.TryParse(value, format, style, out result))
                return result;

            return default(DateTime);
        }
        public static DateTime AsDateTime(string value)
        {
            DateTime result;
            if (DateTime.TryParse(value, out result))
                return result;

            return default(DateTime);
        }
        public static Decimal AsDecimal(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Decimal result;
            if (Decimal.TryParse(value, style, format, out result))
                return result;

            return default(Decimal);
        }
        public static Decimal AsDecimal(string value)
        {
            Decimal result;
            if (Decimal.TryParse(value, out result))
                return result;

            return default(Decimal);
        }
        public static Double AsDouble(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Double result;
            if (Double.TryParse(value, style, format, out result))
                return result;

            return default(Double);
        }
        public static Double AsDouble(string value)
        {
            Double result;
            if (Double.TryParse(value, out result))
                return result;

            return default(Double);
        }
        public static Int16 AsInt16(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Int16 result;
            if (Int16.TryParse(value, style, format, out result))
                return result;

            return default(Int16);
        }
        public static Int16 AsInt16(string value)
        {
            Int16 result;
            if (Int16.TryParse(value, out result))
                return result;

            return default(Int16);
        }
        public static Int32 AsInt32(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Int32 result;
            if (Int32.TryParse(value, style, format, out result))
                return result;

            return default(Int32);
        }
        public static Int32 AsInt32(string value)
        {
            Int32 result;
            if (Int32.TryParse(value, out result))
                return result;

            return default(Int32);
        }
        public static Int64 AsInt64(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Int64 result;
            if (Int64.TryParse(value, style, format, out result))
                return result;

            return default(Int64);
        }
        public static Int64 AsInt64(string value)
        {
            Int64 result;
            if (Int64.TryParse(value, out result))
                return result;

            return default(Int64);
        }
        public static SByte AsSByte(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            SByte result;
            if (SByte.TryParse(value, style, format, out result))
                return result;

            return default(SByte);
        }
        public static SByte AsSByte(string value)
        {
            SByte result;
            if (SByte.TryParse(value, out result))
                return result;

            return default(SByte);
        }
        public static Single AsSingle(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            Single result;
            if (Single.TryParse(value, style, format, out result))
                return result;

            return default(Single);
        }
        public static Single AsSingle(string value)
        {
            Single result;
            if (Single.TryParse(value, out result))
                return result;

            return default(Single);
        }
        public static UInt16 AsUInt16(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            UInt16 result;
            if (UInt16.TryParse(value, style, format, out result))
                return result;

            return default(UInt16);
        }
        public static UInt16 AsUInt16(string value)
        {
            UInt16 result;
            if (UInt16.TryParse(value, out result))
                return result;

            return default(UInt16);
        }
        public static UInt32 AsUInt32(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            UInt32 result;
            if (UInt32.TryParse(value, style, format, out result))
                return result;

            return default(UInt32);
        }
        public static UInt32 AsUInt32(string value)
        {
            UInt32 result;
            if (UInt32.TryParse(value, out result))
                return result;

            return default(UInt32);
        }
        public static UInt64 AsUInt64(string value, System.Globalization.NumberStyles style, IFormatProvider format)
        {
            UInt64 result;
            if (UInt64.TryParse(value, style, format, out result))
                return result;

            return default(UInt64);
        }
        public static UInt64 AsUInt64(string value)
        {
            UInt64 result;
            if (UInt64.TryParse(value, out result))
                return result;

            return default(UInt64);
        }
    }
}