﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains common floating-point math operations
    /// </summary>
    /// <remarks>To be used as a floating-point wrapper around System.Math</remarks>
    public static class MathF
    {
        // These are used for GetHashCode() implementations
        // https://archive.is/YH3UT :: https://stackoverflow.com/questions/38280843/how-to-pick-prime-numbers-to-calculate-the-hash-code

        internal const int m_hash1 = 13;
        internal const int m_hash2 = 31;

        #region public constants

        public const float E = 2.7182818284590452353602874713527f;
        public const float Ln10 = 2.3025850929940456840179914546844f;
        public const float Ln2 = 0.69314718055994530941723212145818f;
        public const float Log10E = 0.43429448190325182765112891891661f;
        public const float Log2E = 1.44269504088896340735992468100189f;
        public const float Sqrt2 = 1.4142135623730950488016887242097f;

        public const float Pi = 3.1415926535897932384626433832795f;
        public const float PiOver2 = 1.5707963267948966192313216916398f;
        public const float PiOver4 = 0.78539816339744830961566084581988f;
        public const float TwoPi = 6.283185307179586476925286766559f;
        
        public const float Tau = 6.283185307179586476925286766559f;
        public const float Phi = 1.6180339887498948482045868343656f;
        public const float Fib618 = 0.61803398874989484820458683436564f;
        public const float Fib382 = 0.38196601125010515179541316563436f;

        public const float DegToRad = (float)(Math.PI / 180.00d);
        public const float RadToDeg = (float)(180.00d / Math.PI);

        #endregion
        #region public static methods

        public static float Acos(double value)
        {
            return (float)Math.Acos(value);
        }
        public static float Asin(double value)
        {
            return (float)Math.Asin(value);
        }
        public static float Atan(double value)
        {
            return (float)Math.Atan(value);
        }
        public static float Atan2(double y, double x)
        {
            return (float)Math.Atan2(y, x);
        }
        public static float Cos(double value)
        {
            return (float)Math.Cos(value);
        }
        public static float Cosh(double value)
        {
            return (float)Math.Cosh(value);
        }
        public static float Sin(double value)
        {
            return (float)Math.Sin(value);
        }
        public static float Sinh(double value)
        {
            return (float)Math.Sinh(value);
        }
        public static float Tan(double value)
        {
            return (float)Math.Tan(value);
        }
        public static float Tanh(double value)
        {
            return (float)Math.Tanh(value);
        }
        
        public static float Abs(double value)
        {
            return (float)Math.Abs(value);
        }
        public static float Ceiling(double value)
        {
            return (float)Math.Ceiling(value);
        }
        public static float Clamp(double value, double min, double max)
        {
            if (value <= min)
                return (float)min;
            else if (value >= max)
                return (float)max;
            else
                return (float)value;
        }
        public static float Exp(double value)
        {
            return (float)Math.Exp(value);
        }
        public static float Floor(double value)
        {
            return (float)Math.Floor(value);
        }
        public static float IEEERemainder(double x, double y)
        {
            return (float)Math.IEEERemainder(x, y);
        }
        public static float Lerp(double value, double other, double percent)
        {
            return (float)(percent * (value - other) + value);
        }
        public static float Log(double value)
        {
            return (float)Math.Log(value);
        }
        public static float Log(double value, double newBase)
        {
            return (float)Math.Log(value, newBase);
        }
        public static float Log10(double value)
        {
            return (float)Math.Log10(value);
        }
        public static float Max(double value, double other)
        {
            if (value >= other)
                return (float)value;
            else
                return (float)other;
        }
        public static float Min(double value, double other)
        {
            if (value <= other)
                return (float)value;
            else
                return (float)other;
        }
        public static float Round(double value)
        {
            return (float)Math.Round(value);
        }
        public static float Power(double x, double y)
        {
            return (float)Math.Pow(x, y);
        }
        public static float Sign(double value)
        {
            return (float)Math.Sign(value);
        }
        public static float Sqrt(double value)
        {
            return (float)Math.Sqrt(value);
        }
        public static float ToRadians(double value)
        {
            const double degToRad = Math.PI / 180.00d;
            return (float)(value * degToRad);
        }
        public static float ToDegrees(double value)
        {
            const double radToDeg = 180.00d / Math.PI;
            return (float)(value * radToDeg);
        }
        public static float Truncate(double value)
        {
            return (float)Math.Truncate(value);
        }

        #endregion
    }
}