﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Collections
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// A heap which automatically sorts itself as elements are added or removed
    /// </summary>
    /// <typeparam name="T">Must be IComparable</typeparam>
    /// <remarks>To be used for data structures such as heaps and priority queues</remarks>
    public class Heap<T> :
        ICollection<T>
        where T : IComparable<T>
    {
        #region protected fields

        protected IComparer<T> m_Comparer;
        protected bool m_IsAscending;

        protected T[] m_Values;
        protected int m_Count;

        #endregion

        #region public constructors

        public Heap(int capacity, bool isAscending, IComparer<T> comparer)
        {
            if (capacity < 0)
                capacity = 0;

            m_Comparer = (comparer ?? Comparer<T>.Default);
            m_IsAscending = isAscending;

            m_Values = new T[capacity + 1];
            m_Count = 0;
        }

        public Heap(bool isAscending) :
            this(0, isAscending, null)
        { }

        public Heap() :
            this(0, false, null)
        { }

        public Heap(IEnumerable<T> values, bool isAscending, IComparer<T> comparer) :
            this(0, isAscending, comparer)
        {
            if (values == null)
                return;

            if (values is IList<T>)
            {
                var other = (IList<T>)values;

                m_Values = new T[other.Count + 1];
                m_Count = other.Count;

                other.CopyTo(m_Values, 1);
            }
            else
            {
                foreach (var ITEM in values)
                    Add(ITEM);
            }
        }

        public Heap(IEnumerable<T> values, bool isAscending) :
            this(values, isAscending, null)
        { }

        #endregion
        #region public properties

        public int Count
        {
            get { return m_Count; }
        }
        public bool IsAscending
        {
            get { return m_IsAscending; }
        }
        public bool IsReadOnly
        {
            get { return false; }
        }

        #endregion
        #region public methods

        public T Pop()
        {
            if (m_Count <= 0)
                throw new InvalidOperationException();

            T result = m_Values[1];
            Remove(result);

            return result;
        }
        public T Peek()
        {
            if (m_Count <= 0)
                throw new InvalidOperationException();

            return m_Values[1];
        }
        public void Sort()
        {
            if (m_Count <= 1)
                return;

            Array.Sort(m_Values, 1, m_Count);
            
            if (!m_IsAscending)
                Array.Reverse(m_Values, 1, m_Count);
        }
        public void Trim()
        {
            Array.Resize(ref m_Values, m_Count + 1);
        }

        #endregion
        #region public methods (object)

        public override string ToString()
        {
            return String.Format(
                "[{0}] Count({1}) IsReadOnly({2}) IsAscending({3})",
                GetType().Name, Count, IsReadOnly, IsAscending);
        }

        #endregion
        #region public methods (ICollection)

        public void Add(T value)
        {
            if (m_Count >= m_Values.Length - 1)
            {
                T[] values = m_Values;
                m_Values = new T[m_Values.GetNextSize()];
                Array.Copy(values, m_Values, values.Length);
            }

            m_Count++;
            m_Values[m_Count] = value;

            int n = m_Count;
            while (n > 1 && m_Values[n].CompareTo(m_Values[n / 2]) < 0 ^ !m_IsAscending)
            {
                var swap = m_Values[n / 2];
                m_Values[n / 2] = m_Values[n];
                m_Values[n] = swap;

                n /= 2;
            }
        }
        public void Clear()
        {
            Array.Clear(m_Values, 1, m_Count);
            m_Count = 0;
        }
        public bool Contains(T value)
        {
            return Array.IndexOf(m_Values, value, 1, m_Count) >= 0;
        }
        public void CopyTo(T[] target, int index)
        {
            Array.Copy(m_Values, 1, target, index, m_Count);
        }
        public bool Remove(T value)
        {
            int n = Array.IndexOf(m_Values, value, 1, m_Count);
            if (n >= 0)
            {
                m_Values[n] = m_Values[m_Count];
                m_Values[m_Count] = default(T);
                m_Count--;

                while (n * 2 <= m_Count)
                {
                    n *= 2;

                    if (n + 1 <= m_Count &&
                        m_Values[n].CompareTo(m_Values[n + 1]) > 0 ^ !m_IsAscending)
                    {
                        n++;
                    }

                    if (m_Values[n].CompareTo(m_Values[n / 2]) < 0 ^ !m_IsAscending)
                    {
                        var swap = m_Values[n / 2];
                        m_Values[n / 2] = m_Values[n];
                        m_Values[n] = swap;
                    }
                }

                return true;
            }

            return false;
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < m_Count; i++)
                yield return m_Values[i + 1];
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }

    /// <summary>
    /// A heap which automatically sorts itself as elements are added or removed
    /// </summary>
    /// <typeparam name="TKey">Must be IComparable</typeparam>
    /// <typeparam name="TValue">Can be anything</typeparam>
    /// <remarks>To be used for data structures such as heaps and priority queues</remarks>
    public class Heap<TKey, TValue> :
        Heap<kvp<TKey, TValue>>
        where TKey : IComparable<TKey>
    {
        #region public constructors

        public Heap(int capacity, bool isAscending, IComparer<kvp<TKey, TValue>> comparer) :
            base(capacity, isAscending, comparer)
        { }

        public Heap(bool isAscending) :
            base(0, isAscending, null)
        { }

        public Heap() :
            base(0, false, null)
        { }

        public Heap(IEnumerable<kvp<TKey, TValue>> values, bool isAscending, IComparer<kvp<TKey, TValue>> comparer) :
            base(values, isAscending, comparer)
        { }

        public Heap(IEnumerable<kvp<TKey, TValue>> values, bool isAscending) :
            base(values, isAscending, null)
        { }

        #endregion
        #region public methods

        public void Add(TKey key, TValue value)
        {
            Add(new kvp<TKey, TValue>(key, value));
        }
        public bool Contains(TKey key, TValue value)
        {
            return Contains(new kvp<TKey, TValue>(key, value));
        }
        public bool ContainsKey(TKey key)
        {
            for (int i = 1; i < Count; i++)
            {
                if (key.Equals(m_Values[i].A))
                    return true;
            }

            return false;
        }
        public bool ContainsValue(TValue value)
        {
            for (int i = 1; i < Count; i++)
            {
                if (value.Equals(m_Values[i].B))
                    return true;
            }

            return false;
        }
        public bool Remove(TKey key, TValue value)
        {
            return Remove(new kvp<TKey, TValue>(key, value));
        }
        public bool Remove(TKey key)
        {
            for (int i = 1; i < Count; i++)
            {
                if (key.Equals(m_Values[i].A))
                    return Remove(m_Values[i]);
            }

            return false;
        }

        #endregion
    }
}