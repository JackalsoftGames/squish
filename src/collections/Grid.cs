﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Collections
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains methods to interpret and manipulate array data as a rectangular region
    /// </summary>
    /// <remarks>To be used with a single-dimension array</remarks>
    public static class Grid
    {
        // NOTE:
        // Values and indexes are interpreted with origin at top left of the screen
        // - 1.) Offset: First, ignore the first N values of the array
        // - 2.) Stride: Next, arrange the remaining values columns of a given width
        // - 3.) Bounds: Finally, limit the operation to a region within those columns
        //
        // Example:
        //   0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L
        //
        // Offset (3):
        //   3 4 5 6 7 8 9 A B C D E F G H I J K L
        //
        // Stride (4):
        //   3 4 5 6
        //   7 8 9 A
        //   B C D E
        //   F G H I
        //   J K L
        //
        // Bounds (1,1,5,5) => (1,1,3,3):
        //   8 9 A
        //   C D E
        //   G H I

        #region public static methods

        public static bool TryGetBounds<T>(T[] values, recti bounds, int stride, int offset,
            out recti result)
        {
            if (values == null || values.Length <= 0 ||
                bounds.W <= 0 || bounds.H <= 0 ||
                stride <= 0 || offset < 0)
            {
                result = recti.Zero;
                return false;
            }

            recti array = new recti(0, 0, stride, ((values.Length - offset) / stride));
            recti.Intersection(ref bounds, ref array, out result);
            return true;
        }

        public static IList<int> GetIndexes<T>(T[] values, recti bounds, int stride, int offset)
        {
            if (!TryGetBounds(values, bounds, stride, offset, out bounds))
                return new List<int>();

            List<int> result = new List<int>();
            for (int y = bounds.Y; y < bounds.Y + bounds.H; y++)
            {
                int n = y * stride + offset;
                for (int x = bounds.X; x < bounds.X + bounds.W; x++)
                    result.Add(n + x);
            }

            return result;
        }
        public static IList<int> GetIndexes<T>(T[] values, recti bounds)
        {
            return GetIndexes(values, bounds, bounds.W, 0);
        }
        public static IList<int> GetIndexes<T>(T[] values, v2i position, v2i size, int stride, int offset)
        {
            return GetIndexes(values, new recti(position, size), stride, offset);
        }
        public static IList<int> GetIndexes<T>(T[] values, v2i position, v2i size)
        {
            return GetIndexes(values, new recti(position, size), size.X, 0);
        }
        public static IList<int> GetIndexes<T>(T[] values, int x, int y, int w, int h, int stride, int offset)
        {
            return GetIndexes(values, new recti(x, y, w, h), stride, offset);
        }
        public static IList<int> GetIndexes<T>(T[] values, int x, int y, int w, int h)
        {
            return GetIndexes(values, new recti(x,y,w,h), w, 0);
        }

        public static Grid<T> Create<T>(T[] values, recti bounds, int stride, int offset)
        {
            if (!TryGetBounds(values, bounds, stride, offset, out bounds))
                return new Grid<T>();

            Grid<T> result = new Grid<T>(
                new T[bounds.W * bounds.H],
                v2i.Zero,
                bounds.Size,
                bounds.Size.X, 0);

            for (int i = 0; i < bounds.H; i++)
                Array.Copy(values, (bounds.Y + i) * stride + bounds.X + offset, result.Values, i * bounds.W, bounds.W);

            return result;
        }
        public static Grid<T> Create<T>(T[] values, recti bounds)
        {
            return Create(values, bounds, bounds.W, 0);
        }
        public static Grid<T> Create<T>(T[] values, v2i position, v2i size, int stride, int offset)
        {
            return Create(values, new recti(position, size), stride, offset);
        }
        public static Grid<T> Create<T>(T[] values, v2i position, v2i size)
        {
            return Create(values, new recti(position, size), size.X, 0);
        }
        public static Grid<T> Create<T>(T[] values, int x, int y, int w, int h, int stride, int offset)
        {
            return Create(values, new recti(x, y, w, h), stride, offset);
        }
        public static Grid<T> Create<T>(T[] values, int x, int y, int w, int h)
        {
            return Create(values, new recti(x, y, w, h), w, 0);
        }

        public static void Fill<T>(T[] values, recti bounds, int stride, int offset, T value)
        {
            if (!TryGetBounds(values, bounds, stride, offset, out bounds))
                return;

            for (int i = 0; i < bounds.W; i++)
                values[(bounds.Y * stride) + bounds.X + i] = value;

            for (int i = 0; i < bounds.H - 1; i++)
                Array.Copy(
                    values, (bounds.Y + i) * stride + bounds.X,
                    values, (bounds.Y + i + 1) * stride + bounds.X,
                    bounds.W);
        }
        public static void Fill<T>(T[] values, recti bounds, T value)
        {
            Fill(values, bounds, bounds.W, 0, value);
        }
        public static void Fill<T>(T[] values, v2i position, v2i size, int stride, int offset, T value)
        {
            Fill(values, new recti(position, size), stride, offset, value);
        }
        public static void Fill<T>(T[] values, v2i position, v2i size, T value)
        {
            Fill(values, new recti(position, size), size.X, 0, value);
        }
        public static void Fill<T>(T[] values, int x, int y, int w, int h, int stride, int offset, T value)
        {
            Fill(values, new recti(x, y, w, h), stride, offset, value);
        }
        public static void Fill<T>(T[] values, int x, int y, int w, int h, T value)
        {
            Fill(values, new recti(x, y, w, h), w, 0, value);
        }

        public static void Flip<T>(T[] values, recti bounds, int stride, int offset, bool horizontal, bool vertical)
        {
            if (!TryGetBounds(values, bounds, stride, offset, out bounds))
                return;

            if (horizontal)
            {
                for (int i = 0; i < bounds.H; i++)
                    Array.Reverse(values, (bounds.Y + i) * stride + bounds.X, bounds.W);
            }

            if (vertical)
            {
                T[] buffer = new T[bounds.W];
                for (int i = 0; i < bounds.H / 2; i++)
                {
                    int index1 = (bounds.Y + i) * stride + bounds.X;
                    int index2 = (bounds.Y + bounds.H - i - 1) * stride + bounds.X;

                    Array.Copy(values, index1, buffer, 0, bounds.W);
                    Array.Copy(values, index2, values, index1, bounds.W);
                    Array.Copy(buffer, 0, values, index2, bounds.W);
                }
            }
        }
        public static void Flip<T>(T[] values, recti bounds, bool horizontal, bool vertical)
        {
            Flip(values, bounds, bounds.W, 0, horizontal, vertical);
        }
        public static void Flip<T>(T[] values, v2i position, v2i size, int stride, int offset, bool horizontal, bool vertical)
        {
            Flip(values, new recti(position, size), stride, offset, horizontal, vertical);
        }
        public static void Flip<T>(T[] values, v2i position, v2i size, bool horizontal, bool vertical)
        {
            Flip(values, new recti(position, size), size.X, 0, horizontal, vertical);
        }
        public static void Flip<T>(T[] values, int x, int y, int w, int h, int stride, int offset, bool horizontal, bool vertical)
        {
            Flip(values, new recti(x, y, w, h), stride, offset, horizontal, vertical);
        }
        public static void Flip<T>(T[] values, int x, int y, int w, int h, bool horizontal, bool vertical)
        {
            Flip(values, new recti(x, y, w, h), w, 0, horizontal, vertical);
        }

        public static void Rotate<T>(T[] values, recti bounds, int stride, int offset, bool clockwise)
        {
            if (clockwise)
            {
                Transpose(values, bounds, stride, offset);
                Flip(values, bounds, stride, offset, true, false);
            }
            else
            {
                Flip(values, bounds, stride, offset, true, false);
                Transpose(values, bounds, stride, offset);
            }
        }
        public static void Rotate<T>(T[] values, recti bounds, bool clockwise)
        {
            Rotate(values, bounds, bounds.W, 0, clockwise);
        }
        public static void Rotate<T>(T[] values, v2i position, v2i size, int stride, int offset, bool clockwise)
        {
            Rotate(values, new recti(position, size), stride, offset, clockwise);
        }
        public static void Rotate<T>(T[] values, v2i position, v2i size, bool clockwise)
        {
            Rotate(values, new recti(position, size), size.X, 0, clockwise);
        }
        public static void Rotate<T>(T[] values, int x, int y, int w, int h, int stride, int offset, bool clockwise)
        {
            Rotate(values, new recti(x, y, w, h), stride, offset, clockwise);
        }
        public static void Rotate<T>(T[] values, int x, int y, int w, int h, bool clockwise)
        {
            Rotate(values, new recti(x, y, w, h), w, 0, clockwise);
        }

        public static void Shift<T>(T[] values, recti bounds, int stride, int offset, int dx, int dy)
        {
            if (!Grid.TryGetBounds(values, bounds, stride, offset, out bounds))
                return;

            dx %= bounds.W;
            if (dx != 0)
            {
                if (dx >= 0)
                    dx = bounds.W - dx;

                T[] buffer = new T[Math.Abs(dx)];
                for (int i = 0; i < bounds.H; i++)
                {
                    int index1 = (bounds.Y + i) * stride + bounds.X;
                    int index2 = index1 + buffer.Length;

                    Array.Copy(values, index1, buffer, 0, buffer.Length);
                    Array.Copy(values, index2, values, index1, bounds.W - buffer.Length);
                    Array.Copy(buffer, 0, values, index1 + bounds.W - buffer.Length, buffer.Length);
                }
            }

            dy %= bounds.H;
            if (dy != 0)
            {
                if (dy < 0)
                    dy += bounds.H;

                T[] buffer = new T[bounds.W * bounds.H];
                for (int i = 0; i < bounds.H; i++)
                    Array.Copy(values, (bounds.Y + i) * stride + bounds.X, buffer, i * bounds.W, bounds.W);

                for (int i = 0; i < bounds.H; i++)
                {
                    Array.Copy(
                        buffer, i * stride,
                        values, (bounds.Y + i + dy) % bounds.H * stride + bounds.X,
                        bounds.W);
                }
            }
        }
        public static void Shift<T>(T[] values, recti bounds, int dx, int dy)
        {
            Shift(values, bounds, bounds.W, 0, dx, dy);
        }
        public static void Shift<T>(T[] values, v2i position, v2i size, int stride, int offset, int dx, int dy)
        {
            Shift(values, new recti(position, size), stride, offset, dx, dy);
        }
        public static void Shift<T>(T[] values, v2i position, v2i size, int dx, int dy)
        {
            Shift(values, new recti(position, size), size.X, 0, dx, dy);
        }
        public static void Shift<T>(T[] values, int x, int y, int w, int h, int stride, int offset, int dx, int dy)
        {
            Shift(values, new recti(x, y, w, h), stride, offset, dx, dy);
        }
        public static void Shift<T>(T[] values, int x, int y, int w, int h, int dx, int dy)
        {
            Shift(values, new recti(x, y, w, h), w, 0, dx, dy);
        }

        public static void Transpose<T>(T[] values, recti bounds, int stride, int offset)
        {
            if (!Grid.TryGetBounds(values, bounds, stride, offset, out bounds))
                return;

            T[] buffer = new T[bounds.W * bounds.H];
            int index = 0;

            for (int x = 0; x < bounds.W; x++)
                for (int y = 0; y < bounds.H; y++)
                {
                    buffer[index++] = values[(bounds.Y + y) * stride + (bounds.X + x)];
                }

            for (int i = 0; i < bounds.H; i++)
            {
                Array.Copy(
                    buffer, i * bounds.W,
                    values, (bounds.Y + i) * stride + bounds.X,
                    bounds.W);
            }
        }
        public static void Transpose<T>(T[] values, recti bounds)
        {
            Transpose(values, bounds, bounds.W, 0);
        }
        public static void Transpose<T>(T[] values, v2i position, v2i size, int stride, int offset)
        {
            Transpose(values, new recti(position, size), stride, offset);
        }
        public static void Transpose<T>(T[] values, v2i position, v2i size)
        {
            Transpose(values, new recti(position, size), size.X, 0);
        }
        public static void Transpose<T>(T[] values, int x, int y, int w, int h, int stride, int offset)
        {
            Transpose(values, new recti(x, y, w, h), stride, offset);
        }
        public static void Transpose<T>(T[] values, int x, int y, int w, int h)
        {
            Transpose(values, new recti(x, y, w, h), w, 0);
        }

        #endregion
    }

    /// <summary>
    /// Contains a view around an array, which is interpreted as a rectangular region of data
    /// </summary>
    /// <typeparam name="T">Can be anything</typeparam>
    /// <remarks>To be used with single-dimension arrays</remarks>
    public class Grid<T> :
        IEnumerable<T>
    {
        #region public constructors

        public Grid(Grid<T> other)
        {
            if (other == null)
                return;

            Values = other.Values;
            Bounds = other.Bounds;
            Stride = other.Stride;
            Offset = other.Offset;
        }

        public Grid(T[] values, recti bounds, int stride, int offset)
        {
            Values = values;
            Bounds = bounds;
            Stride = stride;
            Offset = offset;
        }

        public Grid(T[] values, recti bounds) :
            this(values, bounds, bounds.W, 0)
        { }

        public Grid(T[] values, v2i position, v2i size, int stride, int offset) :
            this(values, new recti(position, size), stride, offset)
        { }

        public Grid(T[] values, v2i position, v2i size) :
            this(values, new recti(position, size), size.X, 0)
        { }

        public Grid(T[] values, int x, int y, int w, int h, int stride, int offset) :
            this(values, new recti(x, y, w, h), stride, offset)
        { }

        public Grid(T[] values, int x, int y, int w, int h) :
            this(values, new recti(x, y, w, h), w, 0)
        { }

        public Grid(T[] values, int w, int h) :
            this(values, new recti(0, 0, w, h), w, 0)
        { }

        public Grid(int w, int h) :
            this(new T[w * h], new recti(0, 0, w, h), w, 0)
        { }

        public Grid() :
            this(null, v2i.Zero, v2i.Zero, 0, 0)
        { }

        #endregion
        #region public fields

        public T[] Values;
        public recti Bounds;
        public int Stride;
        public int Offset;

        #endregion
        #region public indexers

        public T this[v2i position]
        {
            get { return Values[(Bounds.X + position.X) + (Bounds.Y + position.Y) * Stride + Offset]; }
            set { Values[(Bounds.X + position.X) + (Bounds.Y + position.Y) * Stride + Offset] = value; }
        }
        public T this[int x, int y]
        {
            get { return Values[(Bounds.X + x) + (Bounds.Y + y) * Stride + Offset]; }
            set { Values[(Bounds.X + x) + (Bounds.Y + y) * Stride + Offset] = value; }
        }

        #endregion
        #region public methods

        public IList<int> GetIndexes()
        {
            return Grid.GetIndexes(Values, Bounds, Stride, Offset);
        }
        public bool Contains(v2i value)
        {
            return
                (value.X >= 0 && value.X < Bounds.W) &&
                (value.Y >= 0 && value.Y < Bounds.H);
        }
        public bool Contains(int x, int y)
        {
            return
                (x >= 0 && x < Bounds.W) &&
                (y >= 0 && y < Bounds.H);
        }
        public int Transform(v2i value)
        {
            return (Bounds.X + value.X) + (Bounds.Y + value.Y) * Stride + Offset;
        }
        public int Transform(int x, int y)
        {
            return (Bounds.X + x) + (Bounds.Y + y) * Stride + Offset;
        }

        public Grid<T> Copy(recti bounds)
        {
            return Grid.Create(Values, bounds, Stride, Offset);
        }
        public Grid<T> Copy(v2i position, v2i size)
        {
            return Grid.Create(Values, new recti(position, size), Stride, Offset);
        }
        public Grid<T> Copy(int x, int y, int w, int h)
        {
            return Grid.Create(Values, new recti(x, y, w, h), Stride, Offset);
        }
        public Grid<T> Copy()
        {
            return Grid.Create(Values, Bounds, Stride, Offset);
        }

        public void Fill(T value)
        {
            Grid.Fill(Values, Bounds, Stride, Offset, value);
        }
        public void Flip(bool horizontal, bool vertical)
        {
            Grid.Flip(Values, Bounds, Stride, Offset, horizontal, vertical);
        }
        public void Rotate(bool clockwise)
        {
            Grid.Rotate(Values, Bounds, Stride, Offset, clockwise);
        }
        public void Shift(int x, int y)
        {
            Grid.Shift(Values, Bounds, Stride, Offset, x, y);
        }
        public void Transpose()
        {
            Grid.Transpose(Values, Bounds, Stride, Offset);
        }

        #endregion
        #region public methods (object)

        public override string ToString()
        {
            return String.Format(
                "[{0}] Bounds({1}) Stride({2}) Offset({3})",
                GetType().Name, Bounds, Stride, Offset);
        }

        #endregion
        #region public methods (IEnumerable)

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var ITEM in GetIndexes())
                yield return Values[ITEM];
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}