﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a 3-dimensional floating-point vector with X, Y, and Z coordinates
    /// </summary>
    /// <remarks>To be used to represent a coordinate point</remarks>
    public struct v3f :
        IEquatable<v3f>
    {
        #region public static operators

        #region op ()
#if SFML
        public static implicit operator Vector3f(v3f a)
        {
            return new Vector3f(a.X, a.Y, a.Z);
        }
        public static implicit operator v3f(Vector3f a)
        {
            return new v3f(a.X, a.Y, a.Z);
        }
#endif
        public static explicit operator v3f(v2i a)
        {
            return new v3f(a.X, a.Y, 0.00f);
        }
        public static explicit operator v3f(v2f a)
        {
            return new v3f(a.X, a.Y, 0.00f);
        }
        public static explicit operator v3f(v4f a)
        {
            return new v3f(a.X, a.Y, a.Z);
        }

        #endregion
        public static v3f operator -(v3f a)
        {
            a.X = -a.X;
            a.Y = -a.Y;
            a.Z = -a.Z;

            return a;
        }
        public static bool operator ==(v3f a, v3f b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y &&
                a.Z == b.Z;
        }
        public static bool operator !=(v3f a, v3f b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y &&
                a.Z == b.Z);
        }

        public static v3f operator +(v3f a, v3f b)
        {
            a.X = a.X + b.X;
            a.Y = a.Y + b.Y;
            a.Z = a.Z + b.Z;

            return a;
        }
        public static v3f operator -(v3f a, v3f b)
        {
            a.X = a.X - b.X;
            a.Y = a.Y - b.Y;
            a.Z = a.Z - b.Z;

            return a;
        }
        public static v3f operator *(v3f a, v3f b)
        {
            a.X = a.X * b.X;
            a.Y = a.Y * b.Y;
            a.Z = a.Z * b.Z;

            return a;
        }
        public static v3f operator /(v3f a, v3f b)
        {
            a.X = a.X / b.X;
            a.Y = a.Y / b.Y;
            a.Z = a.Z / b.Z;

            return a;
        }

        public static v3f operator +(v3f a, float b)
        {
            a.X = a.X + b;
            a.Y = a.Y + b;
            a.Z = a.Z + b;

            return a;
        }
        public static v3f operator -(v3f a, float b)
        {
            a.X = a.X - b;
            a.Y = a.Y - b;
            a.Z = a.Z - b;

            return a;
        }
        public static v3f operator *(v3f a, float b)
        {
            a.X = a.X * b;
            a.Y = a.Y * b;
            a.Z = a.Z * b;

            return a;
        }
        public static v3f operator /(v3f a, float b)
        {
            a.X = a.X / b;
            a.Y = a.Y / b;
            a.Z = a.Z / b;

            return a;
        }

        public static v3f operator +(float a, v3f b)
        {
            b.X = a + b.X;
            b.Y = a + b.Y;
            b.Z = a + b.Z;

            return b;
        }
        public static v3f operator -(float a, v3f b)
        {
            b.X = a - b.X;
            b.Y = a - b.Y;
            b.Z = a - b.Z;

            return b;
        }
        public static v3f operator *(float a, v3f b)
        {
            b.X = a * b.X;
            b.Y = a * b.Y;
            b.Z = a * b.Z;

            return b;
        }
        public static v3f operator /(float a, v3f b)
        {
            b.X = a / b.X;
            b.Y = a / b.Y;
            b.Z = a / b.Z;

            return b;
        }

        public static bool operator >(v3f a, v3f b)
        {
            return
                a.X > b.X &&
                a.Y > b.Y &&
                a.Z > b.Z;
        }
        public static bool operator <(v3f a, v3f b)
        {
            return
                a.X < b.X &&
                a.Y < b.Y &&
                a.Z < b.Z;
        }
        public static bool operator >=(v3f a, v3f b)
        {
            return
                a.X >= b.X &&
                a.Y >= b.Y &&
                a.Z >= b.Z;
        }
        public static bool operator <=(v3f a, v3f b)
        {
            return
                a.X <= b.X &&
                a.Y <= b.Y &&
                a.Z <= b.Z;
        }

        #endregion
        #region public static fields

        public static readonly v3f Zero = new v3f(0, 0, 0);
        public static readonly v3f One = new v3f(1, 1, 1);
        public static readonly v3f Midpoint = new v3f(0.50f, 0.50f, 0.50f);

        public static readonly v3f UnitX = new v3f(1, 0, 0);
        public static readonly v3f UnitY = new v3f(0, 1, 0);
        public static readonly v3f UnitZ = new v3f(0, 0, 1);

        #endregion
        #region public static methods

        public static void Cross(
            ref v3f a, ref v3f b,
            out v3f result)
        {
            float x = +(a.Y * b.Z - b.Y * a.Z);
            float y = -(a.X * b.Z - b.X * a.Z);
            float z = +(a.X * b.Y - b.X * a.Y);

            result.X = x;
            result.Y = y;
            result.Z = z;
        }

        public static void Dot(
            ref v3f a, ref v3f b,
            out float result)
        {
            result =
                a.X * b.X +
                a.Y * b.Y +
                a.Z * b.Z;
        }

        public static void Normalize(
            ref v3f a,
            out v3f result)
        {
            float factor = (float)(1d / Math.Sqrt(
                a.X * a.X +
                a.Y * a.Y +
                a.Z * a.Z));

            result.X = factor * a.X;
            result.Y = factor * a.Y;
            result.Z = factor * a.Z;
        }

        public static void Reflect(
            ref v3f a, ref v3f normal,
            out v3f result)
        {
            float dot = 2f * (
                a.X * normal.X +
                a.Y * normal.Y +
                a.Z * normal.Z);

            result.X = a.X - (normal.X * dot);
            result.Y = a.Y - (normal.Y * dot);
            result.Z = a.Z - (normal.Z * dot);
        }

        public static void Min(
            ref v3f a, ref v3f b,
            out v3f result)
        {
            if (a.X <= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y <= b.Y) result.Y = a.Y; else result.Y = b.Y;
            if (a.Z <= b.Z) result.Z = a.Z; else result.Z = b.Z;
        }

        public static void Min(
            ref v3f a, float b,
            out v3f result)
        {
            if (a.X <= b) result.X = a.X; else result.X = b;
            if (a.Y <= b) result.Y = a.Y; else result.Y = b;
            if (a.Z <= b) result.Z = a.Z; else result.Z = b;
        }

        public static void Min(
            ref v3f a,
            out float result)
        {
            if (a.X <= a.Y && a.X <= a.Z)
                result = a.X;
            else if (a.Y <= a.Z)
                result = a.Y;
            else
                result = a.Z;
        }

        public static void Max(
            ref v3f a, ref v3f b,
            out v3f result)
        {
            if (a.X >= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y >= b.Y) result.Y = a.Y; else result.Y = b.Y;
            if (a.Z >= b.Z) result.Z = a.Z; else result.Z = b.Z;
        }

        public static void Max(
            ref v3f a, float b,
            out v3f result)
        {
            if (a.X >= b) result.X = a.X; else result.X = b;
            if (a.Y >= b) result.Y = a.Y; else result.Y = b;
            if (a.Z >= b) result.Z = a.Z; else result.Z = b;
        }

        public static void Max(
            ref v3f a,
            out float result)
        {
            if (a.X >= a.Y && a.X >= a.Z)
                result = a.X;
            else if (a.Y >= a.Z)
                result = a.Y;
            else
                result = a.Z;
        }

        public static void Clamp(
            ref v3f a, ref v3f min, ref v3f max,
            out v3f result)
        {
            if (a.X <= min.X) result.X = min.X; else if (a.X >= max.X) result.X = max.X; else result.X = a.X;
            if (a.Y <= min.Y) result.Y = min.Y; else if (a.Y >= max.Y) result.Y = max.Y; else result.Y = a.Y;
            if (a.Z <= min.Z) result.Z = min.Z; else if (a.Z >= max.Z) result.Z = max.Z; else result.Z = a.Z;
        }

        public static void Clamp(
            ref v3f a, float min, float max,
            out v3f result)
        {
            if (a.X <= min) result.X = min; else if (a.X >= max) result.X = max; else result.X = a.X;
            if (a.Y <= min) result.Y = min; else if (a.Y >= max) result.Y = max; else result.Y = a.Y;
            if (a.Z <= min) result.Z = min; else if (a.Z >= max) result.Z = max; else result.Z = a.Z;
        }

        public static void Lerp(
            ref v3f a, ref v3f b, float scale,
            out v3f result)
        {
            result.X = scale * (b.X - a.X) + a.X;
            result.Y = scale * (b.Y - a.Y) + a.Y;
            result.Z = scale * (b.Z - a.Z) + a.Z;
        }

        #endregion

        #region public constructors

        public v3f(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public v3f(float value)
        {
            X = value;
            Y = value;
            Z = value;
        }
        public v3f(v2f value, float z)
        {
            X = value.X;
            Y = value.Y;
            Z = z;
        }
        public v3f(v2i value, float z)
        {
            X = value.X;
            Y = value.Y;
            Z = z;
        }

        #endregion
        #region public fields

        public float X;
        public float Y;
        public float Z;

        #endregion
        #region public properties

        public float Length
        {
            get
            {
                return (float)Math.Sqrt(
                    X * X +
                    Y * Y +
                    Z * Z);
            }
        }
        public float LengthSquared
        {
            get
            {
                return
                    X * X +
                    Y * Y +
                    Z * Z;
            }
        }
        public float LengthInverse
        {
            get
            {
                return (float)(1.00d / Math.Sqrt(
                    X * X +
                    Y * Y +
                    Z * Z));
            }
        }

        #endregion
        #region public methods

        public v3f Cross(v3f value)
        {
            v3f result;
            Cross(ref this, ref value, out result);
            return result;
        }
        public v3f Cross(float x, float y, float z)
        {
            return Cross(new v3f(x, y, z));
        }
        public float Dot(v3f value)
        {
            float result;
            Dot(ref this, ref value, out result);
            return result;
        }
        public float Dot(float x, float y, float z)
        {
            return Dot(new v3f(x, y, z));
        }

        public v3f Normalize()
        {
            v3f result;
            Normalize(ref this, out result);
            return result;
        }
        public v3f Reflect(v3f value)
        {
            v3f result;
            Reflect(ref this, ref value, out result);
            return result;
        }
        public v3f Reflect(float x, float y, float z)
        {
            return Reflect(new v3f(x, y, z));
        }

        public v3f Min(v3f value)
        {
            v3f result;
            Min(ref this, ref value, out result);
            return result;
        }
        public v3f Min(float x, float y, float z)
        {
            return Min(new v3f(x, y, z));
        }
        public v3f Min(float value)
        {
            v3f result;
            Min(ref this, value, out result);
            return result;
        }
        public float Min()
        {
            float result;
            Min(ref this, out result);
            return result;
        }

        public v3f Max(v3f value)
        {
            v3f result;
            Max(ref this, ref value, out result);
            return result;
        }
        public v3f Max(float x, float y, float z)
        {
            return Max(new v3f(x, y, z));
        }
        public v3f Max(float value)
        {
            v3f result;
            Max(ref this, value, out result);
            return result;
        }
        public float Max()
        {
            float result;
            Max(ref this, out result);
            return result;
        }

        public v3f Clamp(v3f min, v3f max)
        {
            v3f result;
            Clamp(ref this, ref min, ref max, out result);
            return result;
        }
        public v3f Clamp(float min, float max)
        {
            v3f result;
            Clamp(ref this, min, max, out result);
            return result;
        }

        public v3f Lerp(v3f a, float scale)
        {
            v3f result;
            Lerp(ref this, ref a, scale, out result);
            return result;
        }

        #endregion
        #region public methods (object)

        public bool Equals(v3f value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y &&
                this.Z == value.Z;
        }
        public override bool Equals(object value)
        {
            return (value is v3f) &&
                ((v3f)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                float x = X;
                float y = Y;
                float z = Z;

                unsafe { return (n2 * (n2 * (n2 * n1 + *(int*)&x) + *(int*)&y) + *(int*)&z); }
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2}) Z({3})",
                GetType().Name, X, Y, Z);
        }

        #endregion
    }
}