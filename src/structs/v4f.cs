﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a 4-dimensional floating-point vector with X, Y, Z, and W coordinates
    /// </summary>
    /// <remarks>To be used to support objects such as matrices and quaternions</remarks>
    public struct v4f :
        IEquatable<v4f>
    {
        #region public static operators

        #region op ()

        public static explicit operator v4f(v2i a)
        {
            return new v4f(a.X, a.Y, 0.00f, 0.00f);
        }
        public static explicit operator v4f(v2f a)
        {
            return new v4f(a.X, a.Y, 0.00f, 0.00f);
        }
        public static explicit operator v4f(v3f a)
        {
            return new v4f(a.X, a.Y, a.Z, 0.00f);
        }

        #endregion
        public static v4f operator -(v4f a)
        {
            a.X = -a.X;
            a.Y = -a.Y;
            a.Z = -a.Z;
            a.X = -a.W;

            return a;
        }
        public static bool operator ==(v4f a, v4f b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y &&
                a.Z == b.Z &&
                a.W == b.W;
        }
        public static bool operator !=(v4f a, v4f b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y &&
                a.Z == b.Z &&
                a.W == b.W);
        }

        public static v4f operator +(v4f a, v4f b)
        {
            a.X = a.X + b.X;
            a.Y = a.Y + b.Y;
            a.Z = a.Z + b.Z;
            a.W = a.W + b.W;

            return a;
        }
        public static v4f operator -(v4f a, v4f b)
        {
            a.X = a.X - b.X;
            a.Y = a.Y - b.Y;
            a.Z = a.Z - b.Z;
            a.W = a.W - b.W;

            return a;
        }
        public static v4f operator *(v4f a, v4f b)
        {
            a.X = a.X * b.X;
            a.Y = a.Y * b.Y;
            a.Z = a.Z * b.Z;
            a.W = a.W * b.W;

            return a;
        }
        public static v4f operator /(v4f a, v4f b)
        {
            a.X = a.X / b.X;
            a.Y = a.Y / b.Y;
            a.Z = a.Z / b.Z;
            a.W = a.W / b.W;

            return a;
        }

        public static v4f operator +(v4f a, float b)
        {
            a.X = a.X + b;
            a.Y = a.Y + b;
            a.Z = a.Z + b;
            a.W = a.W + b;

            return a;
        }
        public static v4f operator -(v4f a, float b)
        {
            a.X = a.X - b;
            a.Y = a.Y - b;
            a.Z = a.Z - b;
            a.W = a.W - b;

            return a;
        }
        public static v4f operator *(v4f a, float b)
        {
            a.X = a.X * b;
            a.Y = a.Y * b;
            a.Z = a.Z * b;
            a.W = a.W * b;

            return a;
        }
        public static v4f operator /(v4f a, float b)
        {
            a.X = a.X / b;
            a.Y = a.Y / b;
            a.Z = a.Z / b;
            a.W = a.W / b;

            return a;
        }

        public static v4f operator +(float a, v4f b)
        {
            b.X = a + b.X;
            b.Y = a + b.Y;
            b.Z = a + b.Z;
            b.W = a + b.W;

            return b;
        }
        public static v4f operator -(float a, v4f b)
        {
            b.X = a - b.X;
            b.Y = a - b.Y;
            b.Z = a - b.Z;
            b.W = a - b.W;

            return b;
        }
        public static v4f operator *(float a, v4f b)
        {
            b.X = a * b.X;
            b.Y = a * b.Y;
            b.Z = a * b.Z;
            b.W = a * b.W;

            return b;
        }
        public static v4f operator /(float a, v4f b)
        {
            b.X = a / b.X;
            b.Y = a / b.Y;
            b.Z = a / b.Z;
            b.W = a / b.W;

            return b;
        }

        public static bool operator >(v4f a, v4f b)
        {
            return
                a.X > b.X &&
                a.Y > b.Y &&
                a.Z > b.Z &&
                a.W > b.W;
        }
        public static bool operator <(v4f a, v4f b)
        {
            return
                a.X < b.X &&
                a.Y < b.Y &&
                a.Z < b.Z &&
                a.W < b.W;
        }
        public static bool operator >=(v4f a, v4f b)
        {
            return
                a.X >= b.X &&
                a.Y >= b.Y &&
                a.Z >= b.Z &&
                a.W >= b.W;
        }
        public static bool operator <=(v4f a, v4f b)
        {
            return
                a.X <= b.X &&
                a.Y <= b.Y &&
                a.Z <= b.Z &&
                a.W <= b.W;
        }

        #endregion
        #region public static fields

        public static readonly v4f Zero = new v4f(0, 0, 0, 0);
        public static readonly v4f One = new v4f(1, 1, 1, 1);
        public static readonly v4f Midpoint = new v4f(0.50f, 0.50f, 0.50f, 0.50f);

        public static readonly v4f UnitX = new v4f(1, 0, 0, 0);
        public static readonly v4f UnitY = new v4f(0, 1, 0, 0);
        public static readonly v4f UnitZ = new v4f(0, 0, 1, 0);
        public static readonly v4f UnitW = new v4f(0, 0, 0, 1);

        #endregion
        #region public static methods

        public static void Dot(
            ref v4f a, ref v4f b,
            out float result)
        {
            result =
                a.X * b.X +
                a.Y * b.Y +
                a.Z * b.Z +
                a.W * b.W;
        }

        public static void Normalize(
            ref v4f a,
            out v4f result)
        {
            float factor = (float)(1d / Math.Sqrt(
                a.X * a.X +
                a.Y * a.Y +
                a.Z * a.Z +
                a.W * a.W));

            result.X = factor * a.X;
            result.Y = factor * a.Y;
            result.Z = factor * a.Z;
            result.W = factor * a.W;
        }

        public static void Reflect(
            ref v4f a, ref v4f normal,
            out v4f result)
        {
            float dot = 2f * (
                a.X * normal.X +
                a.Y * normal.Y +
                a.Z * normal.Z +
                a.W * normal.W);

            result.X = a.X - (normal.X * dot);
            result.Y = a.Y - (normal.Y * dot);
            result.Z = a.Z - (normal.Z * dot);
            result.W = a.W - (normal.W * dot);
        }

        public static void Min(
            ref v4f a, ref v4f b,
            out v4f result)
        {
            if (a.X <= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y <= b.Y) result.Y = a.Y; else result.Y = b.Y;
            if (a.Z <= b.Z) result.Z = a.Z; else result.Z = b.Z;
            if (a.W <= b.W) result.W = a.W; else result.W = b.W;
        }

        public static void Min(
            ref v4f a, float b,
            out v4f result)
        {
            if (a.X <= b) result.X = a.X; else result.X = b;
            if (a.Y <= b) result.Y = a.Y; else result.Y = b;
            if (a.Z <= b) result.Z = a.Z; else result.Z = b;
            if (a.W <= b) result.W = a.W; else result.W = b;
        }

        public static void Min(
            ref v4f a,
            out float result)
        {
            if (a.X <= a.Y && a.X <= a.Z && a.X <= a.W)
                result = a.X;
            else if (a.Y <= a.Z && a.Y <= a.W)
                result = a.Y;
            else if (a.Z <= a.W)
                result = a.Z;
            else
                result = a.W;
        }

        public static void Max(
            ref v4f a, ref v4f b,
            out v4f result)
        {
            if (a.X >= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y >= b.Y) result.Y = a.Y; else result.Y = b.Y;
            if (a.Z >= b.Z) result.Z = a.Z; else result.Z = b.Z;
            if (a.W >= b.W) result.W = a.W; else result.W = b.W;
        }

        public static void Max(
            ref v4f a, float b,
            out v4f result)
        {
            if (a.X >= b) result.X = a.X; else result.X = b;
            if (a.Y >= b) result.Y = a.Y; else result.Y = b;
            if (a.Z >= b) result.Z = a.Z; else result.Z = b;
            if (a.W >= b) result.W = a.W; else result.W = b;
        }

        public static void Max(
            ref v4f a,
            out float result)
        {
            if (a.X >= a.Y && a.X >= a.Z && a.X >= a.W)
                result = a.X;
            else if (a.Y >= a.Z && a.Y >= a.W)
                result = a.Y;
            else if (a.Z >= a.W)
                result = a.Z;
            else
                result = a.W;
        }

        public static void Clamp(
            ref v4f a, ref v4f min, ref v4f max,
            out v4f result)
        {
            if (a.X <= min.X) result.X = min.X; else if (a.X >= max.X) result.X = max.X; else result.X = a.X;
            if (a.Y <= min.Y) result.Y = min.Y; else if (a.Y >= max.Y) result.Y = max.Y; else result.Y = a.Y;
            if (a.Z <= min.Z) result.Z = min.Z; else if (a.Z >= max.Z) result.Z = max.Z; else result.Z = a.Z;
            if (a.W <= min.W) result.W = min.W; else if (a.W >= max.W) result.W = max.W; else result.W = a.W;
        }

        public static void Clamp(
            ref v4f a, float min, float max,
            out v4f result)
        {
            if (a.X <= min) result.X = min; else if (a.X >= max) result.X = max; else result.X = a.X;
            if (a.Y <= min) result.Y = min; else if (a.Y >= max) result.Y = max; else result.Y = a.Y;
            if (a.Z <= min) result.Z = min; else if (a.Z >= max) result.Z = max; else result.Z = a.Z;
            if (a.W <= min) result.W = min; else if (a.W >= max) result.W = max; else result.W = a.W;
        }

        public static void Lerp(
            ref v4f a, ref v4f b, float scale,
            out v4f result)
        {
            result.X = scale * (b.X - a.X) + a.X;
            result.Y = scale * (b.Y - a.Y) + a.Y;
            result.Z = scale * (b.Z - a.Z) + a.Z;
            result.W = scale * (b.W - a.W) + a.W;
        }
        
        #endregion

        #region public constructors

        public v4f(float x, float y, float z, float w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }
        public v4f(float value)
        {
            X = value;
            Y = value;
            Z = value;
            W = value;
        }
        public v4f(v3f value, float w)
        {
            X = value.X;
            Y = value.Y;
            Z = value.Z;
            W = w;
        }
        public v4f(v2f value, float z, float w)
        {
            X = value.X;
            Y = value.Y;
            Z = z;
            W = w;
        }
        public v4f(v2i value, float z, float w)
        {
            X = value.X;
            Y = value.Y;
            Z = z;
            W = w;
        }

        #endregion
        #region public fields

        public float X;
        public float Y;
        public float Z;
        public float W;

        #endregion  
        #region public properties

        public float Length
        {
            get
            {
                return (float)Math.Sqrt(
                    X * X +
                    Y * Y +
                    Z * Z +
                    W * W);
            }
        }
        public float LengthSquared
        {
            get
            {
                return
                    X * X +
                    Y * Y +
                    Z * Z +
                    W * W;
            }
        }
        public float LengthInverse
        {
            get
            {
                return (float)(1.00d / Math.Sqrt(
                    X * X +
                    Y * Y +
                    Z * Z +
                    W * W));
            }
        }

        #endregion
        #region public methods

        public float Dot(v4f value)
        {
            float result;
            Dot(ref this, ref value, out result);
            return result;
        }
        public float Dot(float x, float y, float z, float w)
        {
            return Dot(new v4f(x, y, z, w));
        }

        public v4f Normalize()
        {
            v4f result;
            Normalize(ref this, out result);
            return result;
        }
        public v4f Reflect(v4f value)
        {
            v4f result;
            Reflect(ref this, ref value, out result);
            return result;
        }
        public v4f Reflect(float x, float y, float z, float w)
        {
            return Reflect(new v4f(x, y, z, w));
        }

        public v4f Min(v4f value)
        {
            v4f result;
            Min(ref this, ref value, out result);
            return result;
        }
        public v4f Min(float x, float y, float z, float w)
        {
            return Min(new v4f(x, y, z, w));
        }
        public v4f Min(float value)
        {
            v4f result;
            Min(ref this, value, out result);
            return result;
        }
        public float Min()
        {
            float result;
            Min(ref this, out result);
            return result;
        }

        public v4f Max(v4f value)
        {
            v4f result;
            Max(ref this, ref value, out result);
            return result;
        }
        public v4f Max(float x, float y, float z, float w)
        {
            return Max(new v4f(x, y, z, w));
        }
        public v4f Max(float value)
        {
            v4f result;
            Max(ref this, value, out result);
            return result;
        }
        public float Max()
        {
            float result;
            Max(ref this, out result);
            return result;
        }

        public v4f Clamp(v4f min, v4f max)
        {
            v4f result;
            Clamp(ref this, ref min, ref max, out result);
            return result;
        }
        public v4f Clamp(float min, float max)
        {
            v4f result;
            Clamp(ref this, min, max, out result);
            return result;
        }

        public v4f Lerp(v4f value, float scale)
        {
            v4f result;
            Lerp(ref this, ref value, scale, out result);
            return result;
        }

        #endregion
        #region public methods (object)

        public bool Equals(v4f value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y &&
                this.Z == value.Z &&
                this.W == value.W;
        }
        public override bool Equals(object value)
        {
            return (value is v4f) &&
                ((v4f)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                float x = X;
                float y = Y;
                float z = Z;
                float w = W;

                unsafe { return (n2 * (n2 * (n2 * (n2 * n1 + *(int*)&x) + *(int*)&y) + *(int*)&z) + *(int*)&w); }
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2}) Z({3}) W({4})",
                GetType().Name, X, Y, Z, W);
        }

        #endregion
    }
}