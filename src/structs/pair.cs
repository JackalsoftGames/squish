﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains a pair of values
    /// </summary>
    /// <typeparam name="T1">Can be anything</typeparam>
    /// <typeparam name="T2">Can be anything</typeparam>
    /// <remarks>To be used as a compliment to System.Collections.KeyValuePair</remarks>
    public struct pair<T1, T2> :
        IEquatable<pair<T1, T2>>
    {
        #region public static operators

        #region op ()

        public static implicit operator KeyValuePair<T1, T2>(pair<T1, T2> a)
        {
            return new KeyValuePair<T1, T2>(a.A, a.B);
        }
        public static implicit operator pair<T1, T2>(KeyValuePair<T1, T2> a)
        {
            return new pair<T1, T2>(a.Key, a.Value);
        }

        #endregion
        public static bool operator ==(pair<T1, T2> a, pair<T1, T2> b)
        {
            return
                a.A.Equals(b.A) &&
                a.B.Equals(b.B);
        }
        public static bool operator !=(pair<T1, T2> a, pair<T1, T2> b)
        {
            return !(
                a.A.Equals(b.A) &&
                a.B.Equals(b.B));
        }

        #endregion

        #region public constructors

        public pair(T1 a, T2 b)
        {
            A = a;
            B = b;
        }

        #endregion
        #region public fields

        public T1 A;
        public T2 B;

        #endregion
        #region public methods (object)

        public bool Equals(pair<T1, T2> value)
        {
            return
                A.Equals(value.A) &&
                B.Equals(value.B);
        }
        public override bool Equals(object value)
        {
            return (value is pair<T1, T2>) &&
                ((pair<T1, T2>)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                return (n2 * (n2 * n1 + A.GetHashCode()) + B.GetHashCode());
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] A({1}) B({2})",
                GetType().Name, A, B);
        }

        #endregion
    }
}