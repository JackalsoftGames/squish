﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a rectangular region with X, Y, W, H components with integer values
    /// </summary>
    /// <remarks>To be used to represent a rectangular region</remarks>
    public struct recti :
        IEnumerable<v2i>,
        IEquatable<recti>
    {
        #region public static operators

        #region op ()
#if SFML
        public static implicit operator IntRect(recti a)
        {
            return new IntRect(a.X, a.Y, a.W, a.H);
        }
        public static implicit operator recti(IntRect a)
        {
            return new recti(a.Left, a.Top, a.Width, a.Height);
        }
#endif
        public static explicit operator recti(rectf a)
        {
            return new recti((int)a.X, (int)a.Y, (int)a.W, (int)a.H);
        }

        #endregion
        public static bool operator ==(recti a, recti b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y &&
                a.W == b.W &&
                a.H == b.H;
        }
        public static bool operator !=(recti a, recti b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y &&
                a.W == b.W &&
                a.H == b.H);
        }

        public static recti operator +(recti a, int b)
        {
            a.X += b;
            a.Y += b;
            a.W += b;
            a.H += b;

            return a;
        }
        public static recti operator -(recti a, int b)
        {
            a.X -= b;
            a.Y -= b;
            a.W -= b;
            a.H -= b;

            return a;
        }
        public static recti operator *(recti a, int b)
        {
            a.X *= b;
            a.Y *= b;
            a.W *= b;
            a.H *= b;

            return a;
        }
        public static recti operator /(recti a, int b)
        {
            a.X /= b;
            a.Y /= b;
            a.W /= b;
            a.H /= b;

            return a;
        }

        public static recti operator +(int a, recti b)
        {
            b.X += a;
            b.Y += a;
            b.W += a;
            b.H += a;

            return b;
        }
        public static recti operator -(int a, recti b)
        {
            b.X -= a;
            b.Y -= a;
            b.W -= a;
            b.H -= a;

            return b;
        }
        public static recti operator *(int a, recti b)
        {
            b.X *= a;
            b.Y *= a;
            b.W *= a;
            b.H *= a;

            return b;
        }
        public static recti operator /(int a, recti b)
        {
            b.X /= a;
            b.Y /= a;
            b.W /= a;
            b.H /= a;

            return b;
        }

        #endregion
        #region public static fields

        public static readonly recti Zero = new recti(0, 0, 0, 0);
        public static readonly recti One = new recti(0, 0, 1, 1);

        #endregion
        #region public static methods

        public static void Clamp(
            ref recti a, ref v2i b,
            out v2i result)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X)
                    result.X = a.X;
                else if (b.X >= a.X + a.W)
                    result.X = a.X + a.W - 1;
                else
                    result.X = b.X;
            }
            else
            {
                if (b.X >= a.X)
                    result.X = a.X - 1;
                else if (b.X <= a.X + a.W)
                    result.X = a.X + a.W;
                else
                    result.X = b.X;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y)
                    result.Y = a.Y;
                else if (b.Y >= a.Y + a.H)
                    result.Y = a.Y + a.H - 1;
                else
                    result.Y = b.Y;
            }
            else
            {
                if (b.Y >= a.Y)
                    result.Y = a.Y-1;
                else if (b.Y <= a.Y + a.H)
                    result.Y = a.Y + a.H;
                else
                    result.Y = b.Y;
            }
        }

        public static bool Contains(
            ref recti a, ref recti b)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X || b.X >= a.X + a.W) return false;
                if (b.X + b.W < a.X || b.X + b.W >= a.X + a.W) return false;
            }
            else
            {
                if (b.X >= a.X || b.X < a.X + a.W) return false;
                if (b.X + b.W >= a.X || b.X + b.W < a.X + a.W) return false;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y || b.Y >= a.Y + a.H) return false;
                if (b.Y + b.H < a.Y || b.Y + b.H >= a.Y + a.H) return false;
            }
            else
            {
                if (b.Y >= a.Y || b.Y < a.Y + a.H) return false;
                if (b.Y + b.H >= a.Y || b.Y + b.H < a.Y + a.H) return false;
            }

            return true;
        }

        public static bool Contains(
            ref recti a, ref v2i b)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X || b.X >= a.X + a.W) return false;
            }
            else
            {
                if (b.X >= a.X || b.X < a.X + a.W) return false;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y || b.Y >= a.Y + a.H) return false;
            }
            else
            {
                if (b.Y >= a.Y || b.Y < a.Y + a.H) return false;
            }

            return true;
        }

        public static void Flip(
            ref recti a, bool horizontal, bool vertical,
            out recti result)
        {
            if (horizontal)
            {
                result.X = a.X + a.W;
                result.W = -a.W;
            }
            else
            {
                result.X = a.X;
                result.W = a.W;
            }

            if (vertical)
            {
                result.Y = a.Y + a.H;
                result.H = -a.H;
            }
            else
            {
                result.Y = a.Y;
                result.H = a.H;
            }
        }

        public static void Intersection(
            ref recti a, ref recti b,
            out recti result)
        {
            result.X = Math.Max(Math.Min(a.X, a.X + a.W), Math.Min(b.X, b.X + b.W));
            result.W = Math.Min(Math.Max(a.X, a.X + a.W), Math.Max(b.X, b.X + b.W));
            if (result.X >= result.W) { result = new recti(); return; }

            result.Y = Math.Max(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H));
            result.H = Math.Min(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H));
            if (result.Y >= result.H) { result = new recti(); return; }

            // Convert (x2,y2) into (w,h)
            result.W -= result.X;
            result.H -= result.Y;
        }

        public static bool Intersects(
            ref recti a, ref recti b)
        {
            if (Math.Max(Math.Min(a.X, a.X + a.W), Math.Min(b.X, b.X + b.W)) >=
                Math.Min(Math.Max(a.X, a.X + a.W), Math.Max(b.X, b.X + b.W)))
                return false;

            if (Math.Max(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H)) >=
                Math.Min(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H)))
                return false;

            return true;
        }

        public static void Union(
            ref recti a, ref recti b,
            out recti result)
        {
            result.X = Math.Min(Math.Min(a.X, a.X + a.H), Math.Min(b.X, b.X + b.H));
            result.Y = Math.Min(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H));

            result.W = Math.Max(Math.Max(a.X, a.X + a.H), Math.Max(b.X, b.X + b.H)) - result.X;
            result.H = Math.Max(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H)) - result.Y;
        }

        public static void Union(
            ref recti a, ref v2i b,
            out recti result)
        {
            if (b.X < a.X)
            {
                result.W = a.W + a.X - b.X;
                result.X = b.X;
            }
            else if (b.X > a.X + a.W)
            {
                result.W = b.X - a.X;
                result.X = a.X;
            }
            else
            {
                result.W = a.W;
                result.X = a.X;
            }

            if (b.Y < a.Y)
            {
                result.H = a.H + a.Y - b.Y;
                result.Y = b.Y;
            }
            else if (b.Y > a.Y + a.H)
            {
                result.H = b.Y - a.Y;
                result.Y = a.Y;
            }
            else
            {
                result.H = a.H;
                result.Y = a.Y;
            }
        }

        public static recti Union(IList<v2i> values)
        {
            if (values == null || values.Count == 0)
                return new recti();

            v2i p1 = values[0];
            v2i p2 = values[0];

            for (int i = 1; i < values.Count; i++)
            {
                v2i p = values[i];

                if (p.X < p1.X) p1.X = p.X;
                else if (p.X > p2.X) p2.X = p.X;

                if (p.Y < p1.Y) p1.Y = p.Y;
                else if (p.Y > p2.Y) p2.Y = p.Y;
            }

            return new recti(p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
        }

        #endregion

        #region public constructors

        public recti(v2i position, v2i size)
        {
            X = position.X;
            Y = position.Y;
            W = size.X;
            H = size.Y;
        }
        public recti(v2i size)
        {
            X = 0;
            Y = 0;
            W = size.X;
            H = size.Y;
        }
        public recti(int x, int y, int w, int h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
        public recti(int w, int h)
        {
            X = 0;
            Y = 0;
            W = w;
            H = h;
        }

        #endregion
        #region public fields

        public int X;
        public int Y;
        public int W;
        public int H;

        #endregion
        #region public properties

        public v2i Position
        {
            get { return new v2i(X, Y); }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }
        public v2i Size
        {
            get { return new v2i(W, H); }
            set
            {
                W = value.X;
                H = value.Y;
            }
        }
        public v2i Center
        {
            get
            {
                return new v2i(
                    X + W / 2,
                    Y + H / 2);
            }
            set
            {
                X = value.X - (W / 2);
                Y = value.Y - (H / 2);
            }
        }

        public int Left
        {
            get
            {
                if (W >= 0)
                    return X;

                return (X + W);
            }
        }
        public int Right
        {
            get
            {
                if (W >= 0)
                    return (X + W);

                return X;
            }
        }
        public int Top
        {
            get
            {
                if (H >= 0)
                    return (Y + H);

                return Y;
            }
        }
        public int Bottom
        {
            get
            {
                if (H >= 0)
                    return Y;

                return (Y + H);
            }
        }

        #endregion
        #region public methods

        public v2i Clamp(v2i value)
        {
            v2i result;
            Clamp(ref this, ref value, out result);
            return result;
        }
        public v2i Clamp(int x, int y)
        {
            return Clamp(new v2i(x, y));
        }

        public recti Flip(bool horizontal, bool vertical)
        {
            recti result;
            Flip(ref this, horizontal, vertical, out result);
            return result;
        }

        public bool Contains(recti value)
        {
            return Contains(ref this, ref value);
        }
        public bool Contains(v2i value)
        {
            return Contains(ref this, ref value);
        }
        public bool Contains(int x, int y)
        {
            return Contains(new v2i(x, y));
        }

        public recti Intersection(recti value)
        {
            recti result;
            Intersection(ref this, ref value, out result);
            return result;
        }
        public recti Intersection(v2i position, v2i size)
        {
            return Intersection(new recti(position, size));
        }
        public recti Intersection(int x, int y, int w, int h)
        {
            return Intersection(new recti(x, y, w, h));
        }

        public bool Intersects(recti value)
        {
            return Intersects(ref this, ref value);
        }
        public bool Intersects(v2i position, v2i size)
        {
            return Intersects(new recti(position, size));
        }
        public bool Intersects(int x, int y, int w, int h)
        {
            return Intersects(new recti(x, y, w, h));
        }
        
        public recti Union(recti value)
        {
            recti result;
            Union(ref this, ref value, out result);
            return result;
        }
        public recti Union(v2i value)
        {
            recti result;
            Union(ref this, ref value, out result);
            return result;
        }
        public recti Union(int x, int y)
        {
            return Union(new v2i(x, y));
        }

        public v2i[] ToArray()
        {
            v2i[] result = new v2i[W * H];

            int x1, x2, y1, y2;
            x1 = Left;
            x2 = Right;
            y1 = Bottom;
            y2 = Top;

            int n = 0;
            for (int y = y1; y < y2; y++)
                for (int x = x1; x < x2; x++)
                {
                    result[n].X = x;
                    result[n].Y = y;
                    n++;
                }

            return result;
        }
        public IList<v2i> ToList()
        {
            return new List<v2i>(ToArray());
        }
        
        #endregion
        #region public methods (object)

        public bool Equals(recti value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y &&
                this.W == value.W &&
                this.H == value.H;
        }
        public override bool Equals(object value)
        {
            return (value is recti) &&
                ((recti)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                return (n2 * (n2 * (n2 * (n2 * n1 + X) + Y) + W) + H);
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2}) W({3}) H({4})",
                GetType().Name, X, Y, W, H);
        }

        #endregion
        #region public methods (IEnumerable)

        public IEnumerator<v2i> GetEnumerator()
        {
            int x1, x2, y1, y2;
            x1 = Left;
            x2 = Right;
            y1 = Bottom;
            y2 = Top;

            v2i p = v2i.Zero;
            for (int y = y1; y < y2; y++)
                for (int x = x1; x < x2; x++)
                {
                    p.X = x;
                    p.Y = y;

                    yield return p;
                }
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}