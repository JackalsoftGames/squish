﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a 3x3 transformation matrix
    /// </summary>
    /// <remarks>To be used for linear transformations</remarks>
    public struct m33 :
        IEquatable<m33>
    {
        #region public static operators

        #region op ()

#if SFML
        public static implicit operator Transform(m33 a)
        {
            return new Transform(
                a.A.X, a.A.Y, a.A.Z,
                a.B.X, a.B.Y, a.B.Z,
                a.C.X, a.C.Y, a.C.Z);
        }
#endif

        #endregion
        public static m33 operator -(m33 a)
        {
            m33 result;
            a.GetInverse(out result);
            return result;
        }
        public static bool operator ==(m33 a, m33 b)
        {
            return
                a.A.X == b.A.X &&
                a.A.Y == b.A.Y &&
                a.A.Z == b.A.Z &&

                a.B.X == b.B.X &&
                a.B.Y == b.B.Y &&
                a.B.Z == b.B.Z &&

                a.C.X == b.C.X &&
                a.C.Y == b.C.Y &&
                a.C.Z == b.C.Z;
        }
        public static bool operator !=(m33 a, m33 b)
        {
            return !(
                a.A.X == b.A.X &&
                a.A.Y == b.A.Y &&
                a.A.Z == b.A.Z &&

                a.B.X == b.B.X &&
                a.B.Y == b.B.Y &&
                a.B.Z == b.B.Z &&

                a.C.X == b.C.X &&
                a.C.Y == b.C.Y &&
                a.C.Z == b.C.Z);
        }

        public static m33 operator +(m33 a, m33 b)
        {
            a.A.X = a.A.X + b.A.X;
            a.A.Y = a.A.Y + b.A.Y;
            a.A.Z = a.A.Z + b.A.Z;

            a.B.X = a.B.X + b.B.X;
            a.B.Y = a.B.Y + b.B.Y;
            a.B.Z = a.B.Z + b.B.Z;

            a.C.X = a.C.X + b.C.X;
            a.C.Y = a.C.Y + b.C.Y;
            a.C.Z = a.C.Z + b.C.Z;

            return a;
        }
        public static m33 operator -(m33 a, m33 b)
        {
            a.A.X = a.A.X - b.A.X;
            a.A.Y = a.A.Y - b.A.Y;
            a.A.Z = a.A.Z - b.A.Z;

            a.B.X = a.B.X - b.B.X;
            a.B.Y = a.B.Y - b.B.Y;
            a.B.Z = a.B.Z - b.B.Z;

            a.C.X = a.C.X - b.C.X;
            a.C.Y = a.C.Y - b.C.Y;
            a.C.Z = a.C.Z - b.C.Z;

            return a;
        }
        public static m33 operator *(m33 a, m33 b)
        {
            m33 result;

            result.A.X = (a.A.X * b.A.X) + (a.A.Y * b.B.X) + (a.A.Z * b.C.X);
            result.A.Y = (a.A.X * b.A.Y) + (a.A.Y * b.B.Y) + (a.A.Z * b.C.Y);
            result.A.Z = (a.A.X * b.A.Z) + (a.A.Y * b.B.Z) + (a.A.Z * b.C.Z);

            result.B.X = (a.B.X * b.A.X) + (a.B.Y * b.B.X) + (a.B.Z * b.C.X);
            result.B.Y = (a.B.X * b.A.Y) + (a.B.Y * b.B.Y) + (a.B.Z * b.C.Y);
            result.B.Z = (a.B.X * b.A.Z) + (a.B.Y * b.B.Z) + (a.B.Z * b.C.Z);

            result.C.X = (a.C.X * b.A.X) + (a.C.Y * b.B.X) + (a.C.Z * b.C.X);
            result.C.Y = (a.C.X * b.A.Y) + (a.C.Y * b.B.Y) + (a.C.Z * b.C.Y);
            result.C.Z = (a.C.X * b.A.Z) + (a.C.Y * b.B.Z) + (a.C.Z * b.C.Z);

            return result;
        }

        public static m33 operator +(m33 a, float b)
        {
            a.A.X = a.A.X + b;
            a.A.Y = a.A.Y + b;
            a.A.Z = a.A.Z + b;

            a.B.X = a.B.X + b;
            a.B.Y = a.B.Y + b;
            a.B.Z = a.B.Z + b;

            a.C.X = a.C.X + b;
            a.C.Y = a.C.Y + b;
            a.C.Z = a.C.Z + b;

            return a;
        }
        public static m33 operator -(m33 a, float b)
        {
            a.A.X = a.A.X - b;
            a.A.Y = a.A.Y - b;
            a.A.Z = a.A.Z - b;

            a.B.X = a.B.X - b;
            a.B.Y = a.B.Y - b;
            a.B.Z = a.B.Z - b;

            a.C.X = a.C.X - b;
            a.C.Y = a.C.Y - b;
            a.C.Z = a.C.Z - b;

            return a;
        }
        public static m33 operator *(m33 a, float b)
        {
            a.A.X = a.A.X * b;
            a.A.Y = a.A.Y * b;
            a.A.Z = a.A.Z * b;

            a.B.X = a.B.X * b;
            a.B.Y = a.B.Y * b;
            a.B.Z = a.B.Z * b;

            a.C.X = a.C.X * b;
            a.C.Y = a.C.Y * b;
            a.C.Z = a.C.Z * b;

            return a;
        }
        public static m33 operator /(m33 a, float b)
        {
            a.A.X = a.A.X / b;
            a.A.Y = a.A.Y / b;
            a.A.Z = a.A.Z / b;

            a.B.X = a.B.X / b;
            a.B.Y = a.B.Y / b;
            a.B.Z = a.B.Z / b;

            a.C.X = a.C.X / b;
            a.C.Y = a.C.Y / b;
            a.C.Z = a.C.Z / b;

            return a;
        }
        
        public static m33 operator +(float a, m33 b)
        {
            b.A.X = a + b.A.X;
            b.A.Y = a + b.A.Y;
            b.A.Z = a + b.A.Z;

            b.B.X = a + b.B.X;
            b.B.Y = a + b.B.Y;
            b.B.Z = a + b.B.Z;

            b.C.X = a + b.C.X;
            b.C.Y = a + b.C.Y;
            b.C.Z = a + b.C.Z;

            return b;
        }        
        public static m33 operator -(float a, m33 b)
        {
            b.A.X = a - b.A.X;
            b.A.Y = a - b.A.Y;
            b.A.Z = a - b.A.Z;

            b.B.X = a - b.B.X;
            b.B.Y = a - b.B.Y;
            b.B.Z = a - b.B.Z;

            b.C.X = a - b.C.X;
            b.C.Y = a - b.C.Y;
            b.C.Z = a - b.C.Z;

            return b;
        }
        public static m33 operator *(float a, m33 b)
        {
            b.A.X = a * b.A.X;
            b.A.Y = a * b.A.Y;
            b.A.Z = a * b.A.Z;

            b.B.X = a * b.B.X;
            b.B.Y = a * b.B.Y;
            b.B.Z = a * b.B.Z;

            b.C.X = a * b.C.X;
            b.C.Y = a * b.C.Y;
            b.C.Z = a * b.C.Z;

            return b;
        }
        public static m33 operator /(float a, m33 b)
        {
            b.A.X = a / b.A.X;
            b.A.Y = a / b.A.Y;
            b.A.Z = a / b.A.Z;

            b.B.X = a / b.B.X;
            b.B.Y = a / b.B.Y;
            b.B.Z = a / b.B.Z;

            b.C.X = a / b.C.X;
            b.C.Y = a / b.C.Y;
            b.C.Z = a / b.C.Z;

            return b;
        }

        #endregion
        #region public static fields

        public static readonly m33 Identity = new m33(
            new v3f(1, 0, 0),
            new v3f(0, 1, 0),
            new v3f(0, 0, 1));

        #endregion
        #region public static methods

        public static void Create(v2f position, v2f size, v2f origin, float rotation, out m33 result)
        {
            if (rotation == 0.00f)
            {
                result.A.X = size.X;
                result.A.Y = 0.00f;
                result.A.Z = position.X - (size.X * origin.X);

                result.B.X = 0.00f;
                result.B.Y = size.Y;
                result.B.Z = position.Y - (size.Y * origin.Y);
            }
            else
            {
                float cos = (float)Math.Cos(rotation);
                float sin = (float)Math.Sin(rotation);

                result.A.X = +cos * size.X;
                result.A.Y = -sin * size.Y;
                result.A.Z = position.X - (result.A.X * origin.X) - (result.A.Y * origin.Y);

                result.B.X = +sin * size.X;
                result.B.Y = +cos * size.Y;
                result.B.Z = position.Y - (result.B.X * origin.X) - (result.B.Y * origin.Y);
            }

            result.C.X = 0.00f;
            result.C.Y = 0.00f;
            result.C.Z = 1.00f;
        }
        public static m33 Create(v2f position, v2f size, v2f origin, float rotation)
        {
            m33 result;
            Create(position, size, origin, rotation, out result);
            return result;
        }
        public static m33 Create(float x, float y, float w, float h, v2f origin, float rotation)
        {
            m33 result;
            Create(new v2f(x, y), new v2f(w,h), origin, rotation, out result);
            return result;
        }

        public static void CreateRotation(float value, out m33 result)
        {
            float cos = (float)Math.Cos(value);
            float sin = (float)Math.Sin(value);

            result.A.X = +cos;
            result.A.Y = -sin;
            result.A.Z = 0.00f;

            result.B.X = +sin;
            result.B.Y = +cos;
            result.B.Z = 0.00f;

            result.C.X = 0.00f;
            result.C.Y = 0.00f;
            result.C.Z = 1.00f;
        }
        public static m33 CreateRotation(float value)
        {
            m33 result;
            CreateRotation(value, out result);
            return result;
        }

        public static void CreateScale(v2f value, out m33 result)
        {
            result.A = new v3f(value.X, 0, 0);
            result.B = new v3f(0, value.Y, 0);
            result.C = new v3f(0, 0, 1);
        }
        public static m33 CreateScale(v2f value)
        {
            m33 result;
            CreateScale(value, out result);
            return result;
        }
        public static m33 CreateScale(float x, float y)
        {
            m33 result;
            CreateScale(new v2f(x, y), out result);
            return result;
        }

        public static void CreateShear(v2f value, out m33 result)
        {
            result.A = new v3f(1, value.X, 0);
            result.B = new v3f(value.Y, 1, 0);
            result.C = new v3f(0, 0, 1);
        }
        public static m33 CreateShear(v2f value)
        {
            m33 result;
            CreateShear(value, out result);
            return result;
        }
        public static m33 CreateShear(float x, float y)
        {
            m33 result;
            CreateShear(new v2f(x, y), out result);
            return result;
        }

        public static void CreateTranslation(v2f value, out m33 result)
        {
            result.A = new v3f(1, 0, value.X);
            result.B = new v3f(0, 1, value.Y);
            result.C = new v3f(0, 0, 1);
        }
        public static m33 CreateTranslation(v2f value)
        {
            m33 result;
            CreateTranslation(value, out result);
            return result;
        }
        public static m33 CreateTranslation(float x, float y)
        {
            m33 result;
            CreateTranslation(new v2f(x, y), out result);
            return result;
        }

        #endregion

        #region public constructors

        public m33(ref v3f a, ref v3f b, ref v3f c)
        {
            A = a;
            B = b;
            C = c;
        }
        public m33(v3f a, v3f b, v3f c)
        {
            A = a;
            B = b;
            C = c;
        }
        public m33(
            float a1, float a2, float a3,
            float b1, float b2, float b3,
            float c1, float c2, float c3)
        {
            A.X = a1;
            A.Y = a2;
            A.Z = a3;

            B.X = b1;
            B.Y = b2;
            B.Z = b3;

            C.X = c1;
            C.Y = c2;
            C.Z = c3;
        }

        #endregion
        #region public fields

        public v3f A;
        public v3f B;
        public v3f C;

        #endregion
        #region public methods

        public float GetDeterminant()
        {
            return
                A.X * (B.Y * C.Z - B.Z * C.Y) -
                A.Y * (B.X * C.Z - B.Z * C.X) +
                A.Z * (B.X * C.Y - B.Y * C.X);
        }

        public void GetInverse(
            out m33 result)
        {
            float d =
                A.X * (B.Y * C.Z - B.Z * C.Y) -
                A.Y * (B.X * C.Z - B.Z * C.X) +
                A.Z * (B.X * C.Y - B.Y * C.X);

            if (d == 0.00f)
                result = m33.Identity;
            else
            {
                result.A.X = +(B.Y * C.Z - B.Z * C.Y) / d;
                result.A.Y = -(A.Y * C.Z - A.Z * C.Y) / d;
                result.A.Z = +(A.Y * B.Z - A.Z * B.Y) / d;

                result.B.X = -(B.X * C.Z - B.Z * C.X) / d;
                result.B.Y = +(A.X * C.Z - A.Z * C.X) / d;
                result.B.Z = -(A.X * B.Z - A.Z * B.X) / d;

                result.C.X = +(B.X * C.Y - B.Y * C.X) / d;
                result.C.Y = -(A.X * C.Y - A.Y * C.X) / d;
                result.C.Z = +(A.X * B.Y - A.Y * B.X) / d;
            }
        }
        public m33 GetInverse()
        {
            m33 result;
            GetInverse(out result);
            return result;
        }

        public void GetTranspose(
            out m33 result)
        {
            result.A.X = A.X;
            result.A.Y = B.X;
            result.A.Z = C.X;

            result.B.X = A.Y;
            result.B.Y = B.Y;
            result.B.Z = C.Y;

            result.C.X = A.Z;
            result.C.Y = B.Z;
            result.C.Z = C.Z;
        }
        public m33 GetTranspose()
        {
            m33 result;
            GetTranspose(out result);
            return result;
        }

        public void Transform(v2f value,
            out v2f result)
        {
            result.X = (value.X * A.X) + (value.Y * A.Y) + A.Z;
            result.Y = (value.X * B.X) + (value.Y * B.Y) + B.Z;
        }
        public v2f Transform(v2f value)
        {
            return new v2f(
                (value.X * A.X) + (value.Y * A.Y) + A.Z,
                (value.X * B.X) + (value.Y * B.Y) + B.Z);
        }
        public v2f Transform(float x, float y)
        {
            return new v2f(
                (x * A.X) + (y * A.Y) + A.Z,
                (x * B.X) + (y * B.Y) + B.Z);
        }

        #endregion
        #region public methods (object)

        public bool Equals(m33 value)
        {
            return
                this.A.X == value.A.X &&
                this.A.Y == value.A.Y &&
                this.A.Z == value.A.Z &&

                this.B.X == value.B.X &&
                this.B.Y == value.B.Y &&
                this.B.Z == value.B.Z &&

                this.C.X == value.C.X &&
                this.C.Y == value.C.Y &&
                this.C.Z == value.C.Z;
        }
        public override bool Equals(object value)
        {
            return (value is m33) &&
                ((m33)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                return (n2 * (n2 * (n2 * n1 + A.GetHashCode()) + B.GetHashCode()) + C.GetHashCode());
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] A({1}) B({2}) C({3})",
                GetType().Name, A, B, C);
        }

        #endregion
    }
}