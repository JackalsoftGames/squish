﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a 2-dimensional floating-point vector with X and Y coordinates
    /// </summary>
    /// <remarks>To be used to represent a coordinate point</remarks>
    public struct v2f :
        IEquatable<v2f>,
        IComparable<v2f>
    {
        #region public static operators

        #region op ()
#if SFML
        public static implicit operator Vector2f(v2f a)
        {
            return new Vector2f(a.X, a.Y);
        }
        public static implicit operator v2f(Vector2f a)
        {
            return new v2f(a.X, a.Y);
        }
#endif
        public static explicit operator v2f(v2i a)
        {
            return new v2f(a.X, a.Y);
        }
        public static explicit operator v2f(v3f a)
        {
            return new v2f(a.X, a.Y);
        }
        public static explicit operator v2f(v4f a)
        {
            return new v2f(a.X, a.Y);
        }

        #endregion
        public static v2f operator -(v2f a)
        {
            a.X = -a.X;
            a.Y = -a.Y;

            return a;
        }
        public static bool operator ==(v2f a, v2f b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y;
        }
        public static bool operator !=(v2f a, v2f b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y);
        }

        public static v2f operator +(v2f a, v2f b)
        {
            a.X = a.X + b.X;
            a.Y = a.Y + b.Y;

            return a;
        }
        public static v2f operator -(v2f a, v2f b)
        {
            a.X = a.X - b.X;
            a.Y = a.Y - b.Y;

            return a;
        }
        public static v2f operator *(v2f a, v2f b)
        {
            a.X = a.X * b.X;
            a.Y = a.Y * b.Y;

            return a;
        }
        public static v2f operator /(v2f a, v2f b)
        {
            a.X = a.X / b.X;
            a.Y = a.Y / b.Y;

            return a;
        }

        public static v2f operator +(v2f a, float b)
        {
            a.X = a.X + b;
            a.Y = a.Y + b;

            return a;
        }
        public static v2f operator -(v2f a, float b)
        {
            a.X = a.X - b;
            a.Y = a.Y - b;

            return a;
        }
        public static v2f operator *(v2f a, float b)
        {
            a.X = a.X * b;
            a.Y = a.Y * b;

            return a;
        }
        public static v2f operator /(v2f a, float b)
        {
            a.X = a.X / b;
            a.Y = a.Y / b;

            return a;
        }

        public static v2f operator +(float a, v2f b)
        {
            b.X = a + b.X;
            b.Y = a + b.Y;

            return b;
        }
        public static v2f operator -(float a, v2f b)
        {
            b.X = a - b.X;
            b.Y = a - b.Y;

            return b;
        }
        public static v2f operator *(float a, v2f b)
        {
            b.X = a * b.X;
            b.Y = a * b.Y;

            return b;
        }
        public static v2f operator /(float a, v2f b)
        {
            b.X = a / b.X;
            b.Y = a / b.Y;

            return b;
        }

        public static bool operator >(v2f a, v2f b)
        {
            return
                a.X > b.X &&
                a.Y > b.Y;
        }
        public static bool operator <(v2f a, v2f b)
        {
            return
                a.X < b.X &&
                a.Y < b.Y;
        }
        public static bool operator >=(v2f a, v2f b)
        {
            return
                a.X >= b.X &&
                a.Y >= b.Y;
        }
        public static bool operator <=(v2f a, v2f b)
        {
            return
                a.X <= b.X &&
                a.Y <= b.Y;
        }

        #endregion
        #region public static fields

        public static readonly v2f Zero = new v2f(0, 0);
        public static readonly v2f One = new v2f(1, 1);
        public static readonly v2f Midpoint = new v2f(0.50f, 0.50f);

        public static readonly v2f UnitX = new v2f(1, 0);
        public static readonly v2f UnitY = new v2f(0, 1);

        #endregion
        #region public static methods

        public static void Angle(
            ref v2f a, ref v2f b,
            out float result)
        {
            result = (float)Math.Atan2(b.Y - a.Y, b.X - a.X);
        }

        public static void Cross(
            ref v2f a, ref v2f b,
            out float result)
        {
            result = +(a.X * b.Y - b.X * a.Y);
        }
        
        public static void Dot(
            ref v2f a, ref v2f b,
            out float result)
        {
            result =
                a.X * b.X +
                a.Y * b.Y;
        }

        public static void Normalize(
            ref v2f a,
            out v2f result)
        {
            float factor = (float)(1d / Math.Sqrt(
                a.X * a.X +
                a.Y * a.Y));

            result.X = factor * a.X;
            result.Y = factor * a.Y;
        }

        public static void Reflect(
            ref v2f a, ref v2f normal,
            out v2f result)
        {
            float dot = 2f * (
                a.X * normal.X +
                a.Y * normal.Y);

            result.X = a.X - (normal.X * dot);
            result.Y = a.Y - (normal.Y * dot);
        }

        public static void Min(
            ref v2f a, ref v2f b,
            out v2f result)
        {
            if (a.X <= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y <= b.Y) result.Y = a.Y; else result.Y = b.Y;
        }

        public static void Min(
            ref v2f a, float b,
            out v2f result)
        {
            if (a.X <= b) result.X = a.X; else result.X = b;
            if (a.Y <= b) result.Y = a.Y; else result.Y = b;
        }

        public static void Min(
            ref v2f a,
            out float result)
        {
            if (a.X <= a.Y)
                result = a.X;
            else
                result = a.Y;
        }

        public static void Max(
            ref v2f a, ref v2f b,
            out v2f result)
        {
            if (a.X >= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y >= b.Y) result.Y = a.Y; else result.Y = b.Y;
        }

        public static void Max(
            ref v2f a, float b,
            out v2f result)
        {
            if (a.X >= b) result.X = a.X; else result.X = b;
            if (a.Y >= b) result.Y = a.Y; else result.Y = b;
        }
        
        public static void Max(
            ref v2f a,
            out float result)
        {
            if (a.X >= a.Y)
                result = a.X;
            else
                result = a.Y;
        }

        public static void Clamp(
            ref v2f a, ref v2f min, ref v2f max,
            out v2f result)
        {
            if (a.X <= min.X) result.X = min.X; else if (a.X >= max.X) result.X = max.X; else result.X = a.X;
            if (a.Y <= min.Y) result.Y = min.Y; else if (a.Y >= max.Y) result.Y = max.Y; else result.Y = a.Y;
        }

        public static void Clamp(
            ref v2f a, float min, float max,
            out v2f result)
        {
            if (a.X <= min) result.X = min; else if (a.X >= max) result.X = max; else result.X = a.X;
            if (a.Y <= min) result.Y = min; else if (a.Y >= max) result.Y = max; else result.Y = a.Y;
        }

        public static void Lerp(
            ref v2f a, ref v2f b, float scale,
            out v2f result)
        {
            result.X = scale * (b.X - a.X) + a.X;
            result.Y = scale * (b.Y - a.Y) + a.Y;
        }

        #endregion

        #region public constructors

        public v2f(float x, float y)
        {
            X = x;
            Y = y;
        }
        public v2f(float value)
        {
            X = value;
            Y = value;
        }

        #endregion
        #region public fields

        public float X;
        public float Y;

        #endregion
        #region public properties

        public float Length
        {
            get
            {
                return (float)Math.Sqrt(
                    X * X +
                    Y * Y);
            }
        }
        public float LengthSquared
        {
            get
            {
                return
                    X * X +
                    Y * Y;
            }
        }
        public float LengthInverse
        {
            get
            {
                return (float)(1.00d / Math.Sqrt(
                    X * X +
                    Y * Y));
            }
        }

        #endregion
        #region public methods

        public float Angle(v2f value)
        {
            float result;
            Angle(ref this, ref value, out result);
            return result;
        }
        public float Angle(float x, float y)
        {
            return Angle(new v2f(x, y));
        }
        public float Cross(v2f value)
        {
            float result;
            Cross(ref this, ref value, out result);
            return result;
        }
        public float Cross(float x, float y)
        {
            return Cross(new v2f(x, y));
        }
        public float Dot(v2f value)
        {
            float result;
            Dot(ref this, ref value, out result);
            return result;
        }
        public float Dot(float x, float y)
        {
            return Dot(new v2f(x, y));
        }

        public v2f Normalize()
        {
            v2f result;
            Normalize(ref this, out result);
            return result;
        }
        public v2f Reflect(v2f value)
        {
            v2f result;
            Reflect(ref this, ref value, out result);
            return result;
        }
        public v2f Reflect(float x, float y)
        {
            return Reflect(new v2f(x, y));
        }

        public v2f Min(v2f value)
        {
            v2f result;
            Min(ref this, ref value, out result);
            return result;
        }
        public v2f Min(float x, float y)
        {
            return Min(new v2f(x, y));
        }
        public v2f Min(float value)
        {
            v2f result;
            Min(ref this, value, out result);
            return result;
        }
        public float Min()
        {
            float result;
            v2f.Min(ref this, out result);
            return result;
        }

        public v2f Max(v2f value)
        {
            v2f result;
            Max(ref this, ref value, out result);
            return result;
        }
        public v2f Max(float x, float y)
        {
            return Max(new v2f(x, y));
        }
        public v2f Max(float value)
        {
            v2f result;
            Max(ref this, value, out result);
            return result;
        }
        public float Max()
        {
            float result;
            v2f.Max(ref this, out result);
            return result;
        }

        public v2f Clamp(v2f min, v2f max)
        {
            v2f result;
            Clamp(ref this, ref min, ref max, out result);
            return result;
        }
        public v2f Clamp(float min, float max)
        {
            v2f result;
            Clamp(ref this, min, max, out result);
            return result;
        }

        public v2f Lerp(v2f value, float scale)
        {
            v2f result;
            Lerp(ref this, ref value, scale, out result);
            return result;
        }

        #endregion
        #region public methods (object)

        public bool Equals(v2f value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y;
        }
        public override bool Equals(object value)
        {
            return (value is v2f) &&
                ((v2f)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                float x = X;
                float y = Y;

                unsafe { return (n2 * (n2 * n1 + *(int*)&x) + *(int*)&y); }
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2})",
                GetType().Name, X, Y);
        }

        #endregion
        #region public methods (IComparable)

        public int CompareTo(v2f other)
        {
            float a = Y * other.X;
            float b = X * other.Y;

            if (a > b) return +1;
            if (a < b) return -1;
            return 0;
        }

        #endregion
    }
}