﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a rectangular region with X, Y, W, H components with floating-point values
    /// </summary>
    /// <remarks>To be used to represent a rectangular region</remarks>
    public struct rectf :
        IEquatable<rectf>
    {
        #region public static operators

        #region op ()
#if SFML
        public static implicit operator FloatRect(rectf a)
        {
            return new FloatRect(a.X, a.Y, a.W, a.H);
        }
        public static implicit operator rectf(FloatRect a)
        {
            return new rectf(a.Left, a.Top, a.Width, a.Height);
        }
#endif
        public static explicit operator rectf(recti a)
        {
            return new rectf(a.X, a.Y, a.W, a.H);
        }

        #endregion
        public static bool operator ==(rectf a, rectf b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y &&
                a.W == b.W &&
                a.H == b.H;
        }
        public static bool operator !=(rectf a, rectf b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y &&
                a.W == b.W &&
                a.H == b.H);
        }

        public static rectf operator +(rectf a, float b)
        {
            a.X += b;
            a.Y += b;
            a.W += b;
            a.H += b;

            return a;
        }
        public static rectf operator -(rectf a, float b)
        {
            a.X -= b;
            a.Y -= b;
            a.W -= b;
            a.H -= b;

            return a;
        }
        public static rectf operator *(rectf a, float b)
        {
            a.X *= b;
            a.Y *= b;
            a.W *= b;
            a.H *= b;

            return a;
        }
        public static rectf operator /(rectf a, float b)
        {
            a.X /= b;
            a.Y /= b;
            a.W /= b;
            a.H /= b;

            return a;
        }

        public static rectf operator +(float a, rectf b)
        {
            b.X += a;
            b.Y += a;
            b.W += a;
            b.H += a;

            return b;
        }
        public static rectf operator -(float a, rectf b)
        {
            b.X -= a;
            b.Y -= a;
            b.W -= a;
            b.H -= a;

            return b;
        }
        public static rectf operator *(float a, rectf b)
        {
            b.X *= a;
            b.Y *= a;
            b.W *= a;
            b.H *= a;

            return b;
        }
        public static rectf operator /(float a, rectf b)
        {
            b.X /= a;
            b.Y /= a;
            b.W /= a;
            b.H /= a;

            return b;
        }

        #endregion
        #region public static fields

        public static readonly rectf Zero = new rectf(0, 0, 0, 0);
        public static readonly rectf One = new rectf(0, 0, 1, 1);

        #endregion
        #region public static methods

        public static void Clamp(
            ref rectf a, ref v2f b,
            out v2f result)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X)
                    result.X = a.X;
                else if (b.X >= a.X + a.W)
                    result.X = a.X + a.W - 1;
                else
                    result.X = b.X;
            }
            else
            {
                if (b.X >= a.X)
                    result.X = a.X - 1;
                else if (b.X <= a.X + a.W)
                    result.X = a.X + a.W;
                else
                    result.X = b.X;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y)
                    result.Y = a.Y;
                else if (b.Y >= a.Y + a.H)
                    result.Y = a.Y + a.H - 1;
                else
                    result.Y = b.Y;
            }
            else
            {
                if (b.Y >= a.Y)
                    result.Y = a.Y - 1;
                else if (b.Y <= a.Y + a.H)
                    result.Y = a.Y + a.H;
                else
                    result.Y = b.Y;
            }
        }

        public static bool Contains(
            ref rectf a, ref rectf b)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X || b.X >= a.X + a.W) return false;
                if (b.X + b.W < a.X || b.X + b.W >= a.X + a.W) return false;
            }
            else
            {
                if (b.X >= a.X || b.X < a.X + a.W) return false;
                if (b.X + b.W >= a.X || b.X + b.W < a.X + a.W) return false;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y || b.Y >= a.Y + a.H) return false;
                if (b.Y + b.H < a.Y || b.Y + b.H >= a.Y + a.H) return false;
            }
            else
            {
                if (b.Y >= a.Y || b.Y < a.Y + a.H) return false;
                if (b.Y + b.H >= a.Y || b.Y + b.H < a.Y + a.H) return false;
            }

            return true;
        }

        public static bool Contains(
            ref rectf a, ref v2f b)
        {
            if (a.W >= 0)
            {
                if (b.X < a.X || b.X >= a.X + a.W) return false;
            }
            else
            {
                if (b.X >= a.X || b.X < a.X + a.W) return false;
            }

            if (a.H >= 0)
            {
                if (b.Y < a.Y || b.Y >= a.Y + a.H) return false;
            }
            else
            {
                if (b.Y >= a.Y || b.Y < a.Y + a.H) return false;
            }

            return true;
        }

        public static void Flip(
            ref rectf a, bool horizontal, bool vertical,
            out rectf result)
        {
            if (horizontal)
            {
                result.X = a.X + a.W;
                result.W = -a.W;
            }
            else
            {
                result.X = a.X;
                result.W = a.W;
            }

            if (vertical)
            {
                result.Y = a.Y + a.H;
                result.H = -a.H;
            }
            else
            {
                result.Y = a.Y;
                result.H = a.H;
            }
        }

        public static void Intersection(
            ref rectf a, ref rectf b,
            out rectf result)
        {
            result.X = Math.Max(Math.Min(a.X, a.X + a.W), Math.Min(b.X, b.X + b.W));
            result.W = Math.Min(Math.Max(a.X, a.X + a.W), Math.Max(b.X, b.X + b.W));
            if (result.X >= result.W) { result = new rectf(); return; }

            result.Y = Math.Max(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H));
            result.H = Math.Min(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H));
            if (result.Y >= result.H) { result = new rectf(); return; }

            // Convert (x2,y2) into (w,h)
            result.W -= result.X;
            result.H -= result.Y;
        }

        public static bool Intersects(
            ref rectf a, ref rectf b)
        {
            if (Math.Max(Math.Min(a.X, a.X + a.W), Math.Min(b.X, b.X + b.W)) >=
                Math.Min(Math.Max(a.X, a.X + a.W), Math.Max(b.X, b.X + b.W)))
                return false;

            if (Math.Max(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H)) >=
                Math.Min(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H)))
                return false;

            return true;
        }

        public static void Union(
            ref rectf a, ref rectf b,
            out rectf result)
        {
            result.X = Math.Min(Math.Min(a.X, a.X + a.H), Math.Min(b.X, b.X + b.H));
            result.Y = Math.Min(Math.Min(a.Y, a.Y + a.H), Math.Min(b.Y, b.Y + b.H));

            result.W = Math.Max(Math.Max(a.X, a.X + a.H), Math.Max(b.X, b.X + b.H)) - result.X;
            result.H = Math.Max(Math.Max(a.Y, a.Y + a.H), Math.Max(b.Y, b.Y + b.H)) - result.Y;
        }

        public static void Union(
            ref rectf a, ref v2f b,
            out rectf result)
        {
            if (b.X < a.X)
            {
                result.W = a.W + a.X - b.X;
                result.X = b.X;
            }
            else if (b.X > a.X + a.W)
            {
                result.W = b.X - a.X;
                result.X = a.X;
            }
            else
            {
                result.W = a.W;
                result.X = a.X;
            }

            if (b.Y < a.Y)
            {
                result.H = a.H + a.Y - b.Y;
                result.Y = b.Y;
            }
            else if (b.Y > a.Y + a.H)
            {
                result.H = b.Y - a.Y;
                result.Y = a.Y;
            }
            else
            {
                result.H = a.H;
                result.Y = a.Y;
            }
        }

        public static rectf Union(IList<v2f> values)
        {
            if (values == null || values.Count == 0)
                return new rectf();

            v2f p1 = values[0];
            v2f p2 = values[0];

            for (int i = 1; i < values.Count; i++)
            {
                v2f p = values[i];

                if (p.X < p1.X) p1.X = p.X;
                else if (p.X > p2.X) p2.X = p.X;

                if (p.Y < p1.Y) p1.Y = p.Y;
                else if (p.Y > p2.Y) p2.Y = p.Y;
            }

            return new rectf(p1.X, p1.Y, p2.X - p1.X, p2.Y - p1.Y);
        }

        #endregion

        #region public constructors

        public rectf(v2f position, v2f size)
        {
            X = position.X;
            Y = position.Y;
            W = size.X;
            H = size.Y;
        }
        public rectf(v2f size)
        {
            X = 0;
            Y = 0;
            W = size.X;
            H = size.Y;
        }
        public rectf(float x, float y, float w, float h)
        {
            X = x;
            Y = y;
            W = w;
            H = h;
        }
        public rectf(float w, float h)
        {
            X = 0;
            Y = 0;
            W = w;
            H = h;
        }

        #endregion
        #region public fields

        public float X;
        public float Y;
        public float W;
        public float H;

        #endregion
        #region public properties

        public v2f Position
        {
            get { return new v2f(X, Y); }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }
        public v2f Size
        {
            get { return new v2f(W, H); }
            set
            {
                W = value.X;
                H = value.Y;
            }
        }
        public v2f Center
        {
            get
            {
                return new v2f(
                    X + W / 2f,
                    Y + H / 2f);
            }
            set
            {
                X = value.X - (W / 2f);
                Y = value.Y - (H / 2f);
            }
        }

        public float Left
        {
            get
            {
                if (W >= 0)
                    return X;

                return (X + W);
            }
        }
        public float Right
        {
            get
            {
                if (W >= 0)
                    return (X + W);

                return X;
            }
        }
        public float Top
        {
            get
            {
                if (H >= 0)
                    return (Y + H);

                return Y;
            }
        }
        public float Bottom
        {
            get
            {
                if (H >= 0)
                    return Y;

                return (Y + H);
            }
        }

        #endregion
        #region public methods

        public v2f Clamp(v2f value)
        {
            v2f result;
            Clamp(ref this, ref value, out result);
            return result;
        }
        public v2f Clamp(float x, float y)
        {
            return Clamp(new v2f(x, y));
        }

        public rectf Flip(bool horizontal, bool vertical)
        {
            rectf result;
            Flip(ref this, horizontal, vertical, out result);
            return result;
        }
        
        public bool Contains(rectf value)
        {
            return Contains(ref this, ref value);
        }
        public bool Contains(v2f value)
        {
            return Contains(ref this, ref value);
        }
        public bool Contains(float x, float y)
        {
            return Contains(new v2f(x, y));
        }
        
        public rectf Intersection(rectf value)
        {
            rectf result;
            Intersection(ref this, ref value, out result);
            return result;
        }
        public rectf Intersection(v2f position, v2f size)
        {
            return Intersection(new rectf(position, size));
        }
        public rectf Intersection(float x, float y, float w, float h)
        {
            return Intersection(new rectf(x, y, w, h));
        }

        public bool Intersects(rectf value)
        {
            return Intersects(ref this, ref value);
        }
        public bool Intersects(v2f position, v2f size)
        {
            return Intersects(new rectf(position, size));
        }
        public bool Intersects(float x, float y, float w, float h)
        {
            return Intersects(new rectf(x, y, w, h));
        }

        public rectf Union(rectf value)
        {
            rectf result;
            Union(ref this, ref value, out result);
            return result;
        }
        public rectf Union(v2f value)
        {
            rectf result;
            Union(ref this, ref value, out result);
            return result;
        }
        public rectf Union(float x, float y)
        {
            return Union(new v2f(x, y));
        }

        #endregion
        #region public methods (object)

        public bool Equals(rectf value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y &&
                this.W == value.W &&
                this.H == value.H;
        }
        public override bool Equals(object value)
        {
            return (value is rectf) &&
                ((rectf)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                float x = X;
                float y = Y;
                float w = W;
                float h = H;

                unsafe { return (n2 * (n2 * (n2 * (n2 * n1 + *(int*)&x) + *(int*)&y) + *(int*)&w) + *(int*)&h); }
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2}) W({3}) H({4})",
                GetType().Name, X, Y, W, H);
        }

        #endregion
    }
}