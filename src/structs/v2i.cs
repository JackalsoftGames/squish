﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Defines a 2-dimensional integer vector with X and Y coordinates
    /// </summary>
    /// <remarks>To be used to represent a coordinate point</remarks>
    public struct v2i :
        IEquatable<v2i>,
        IComparable<v2i>
    {
        #region public static operators

        #region op ()

#if SFML
        public static implicit operator Vector2i(v2i a)
        {
            return new Vector2i(a.X, a.Y);
        }
        public static implicit operator v2i(Vector2i a)
        {
            return new v2i(a.X, a.Y);
        }
#endif
        public static explicit operator v2i(v2f a)
        {
            return new v2i((int)a.X, (int)a.Y);
        }
        public static explicit operator v2i(v3f a)
        {
            return new v2i((int)a.X, (int)a.Y);
        }
        public static explicit operator v2i(v4f a)
        {
            return new v2i((int)a.X, (int)a.Y);
        }

        #endregion
        public static v2i operator -(v2i a)
        {
            a.X = -a.X;
            a.Y = -a.Y;

            return a;
        }
        public static bool operator ==(v2i a, v2i b)
        {
            return
                a.X == b.X &&
                a.Y == b.Y;
        }
        public static bool operator !=(v2i a, v2i b)
        {
            return !(
                a.X == b.X &&
                a.Y == b.Y);
        }

        public static v2i operator +(v2i a, v2i b)
        {
            a.X = a.X + b.X;
            a.Y = a.Y + b.Y;

            return a;
        }
        public static v2i operator -(v2i a, v2i b)
        {
            a.X = a.X - b.X;
            a.Y = a.Y - b.Y;

            return a;
        }
        public static v2i operator *(v2i a, v2i b)
        {
            a.X = a.X * b.X;
            a.Y = a.Y * b.Y;

            return a;
        }
        public static v2i operator /(v2i a, v2i b)
        {
            a.X = a.X / b.X;
            a.Y = a.Y / b.Y;

            return a;
        }
        
        public static v2i operator +(v2i a, int b)
        {
            a.X = a.X + b;
            a.Y = a.Y + b;

            return a;
        }
        public static v2i operator -(v2i a, int b)
        {
            a.X = a.X - b;
            a.Y = a.Y - b;

            return a;
        }
        public static v2i operator *(v2i a, int b)
        {
            a.X = a.X * b;
            a.Y = a.Y * b;

            return a;
        }
        public static v2i operator /(v2i a, int b)
        {
            a.X = a.X / b;
            a.Y = a.Y / b;

            return a;
        }

        public static v2i operator +(int a, v2i b)
        {
            b.X = a + b.X;
            b.Y = a + b.Y;

            return b;
        }
        public static v2i operator -(int a, v2i b)
        {
            b.X = a - b.X;
            b.Y = a - b.Y;

            return b;
        }
        public static v2i operator *(int a, v2i b)
        {
            b.X = a * b.X;
            b.Y = a * b.Y;

            return b;
        }
        public static v2i operator /(int a, v2i b)
        {
            b.X = a / b.X;
            b.Y = a / b.Y;

            return b;
        }

        public static bool operator >(v2i a, v2i b)
        {
            return
                a.X > b.X &&
                a.Y > b.Y;
        }
        public static bool operator <(v2i a, v2i b)
        {
            return
                a.X < b.X &&
                a.Y < b.Y;
        }
        public static bool operator >=(v2i a, v2i b)
        {
            return
                a.X >= b.X &&
                a.Y >= b.Y;
        }
        public static bool operator <=(v2i a, v2i b)
        {
            return
                a.X <= b.X &&
                a.Y <= b.Y;
        }

        #endregion
        #region public static fields

        public static readonly v2i Zero = new v2i(0, 0);
        public static readonly v2i One = new v2i(1, 1);

        public static readonly v2i UnitX = new v2i(1, 0);
        public static readonly v2i UnitY = new v2i(0, 1);

        #endregion
        #region public static methods

        public static void Cross(
            ref v2i a, ref v2i b,
            out int result)
        {
            result = +(a.X * b.Y - b.X * a.Y);
        }

        public static void Dot(
            ref v2i a, ref v2i b,
            out float result)
        {
            result =
                a.X * b.X +
                a.Y * b.Y;
        }

        public static void Min(
            ref v2i a, ref v2i b,
            out v2i result)
        {
            if (a.X <= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y <= b.Y) result.Y = a.Y; else result.Y = b.Y;
        }

        public static void Min(
            ref v2i a, int b,
            out v2i result)
        {
            if (a.X <= b) result.X = a.X; else result.X = b;
            if (a.Y <= b) result.Y = a.Y; else result.Y = b;
        }

        public static void Min(
            ref v2i a,
            out float result)
        {
            if (a.X <= a.Y)
                result = a.X;
            else
                result = a.Y;
        }

        public static void Max(
            ref v2i a, ref v2i b,
            out v2i result)
        {
            if (a.X >= b.X) result.X = a.X; else result.X = b.X;
            if (a.Y >= b.Y) result.Y = a.Y; else result.Y = b.Y;
        }

        public static void Max(
            ref v2i a, int b,
            out v2i result)
        {
            if (a.X >= b) result.X = a.X; else result.X = b;
            if (a.Y >= b) result.Y = a.Y; else result.Y = b;
        }

        public static void Max(
            ref v2i a,
            out float result)
        {
            if (a.X >= a.Y)
                result = a.X;
            else
                result = a.Y;
        }

        public static void Clamp(
            ref v2i a, ref v2i min, ref v2i max,
            out v2i result)
        {
            if (a.X <= min.X) result.X = min.X; else if (a.X >= max.X) result.X = max.X; else result.X = a.X;
            if (a.Y <= min.Y) result.Y = min.Y; else if (a.Y >= max.Y) result.Y = max.Y; else result.Y = a.Y;
        }

        public static void Clamp(
            ref v2i a, int min, int max,
            out v2i result)
        {
            if (a.X <= min) result.X = min; else if (a.X >= max) result.X = max; else result.X = a.X;
            if (a.Y <= min) result.Y = min; else if (a.Y >= max) result.Y = max; else result.Y = a.Y;
        }

        public static void Lerp(
            ref v2i a, ref v2i b, float scale,
            out v2i result)
        {
            result.X = (int)(scale * (b.X - a.X) + a.X);
            result.Y = (int)(scale * (b.Y - a.Y) + a.Y);
        }

        #endregion

        #region public constructors

        public v2i(int x, int y)
        {
            X = x;
            Y = y;
        }
        public v2i(int value)
        {
            X = value;
            Y = value;
        }

        #endregion
        #region public fields

        public int X;
        public int Y;

        #endregion
        #region public properties

        public float Length
        {
            get
            {
                return (float)Math.Sqrt(
                    X * X +
                    Y * Y);
            }
        }
        public float LengthSquared
        {
            get
            {
                return
                    X * X +
                    Y * Y;
            }
        }
        public float LengthInverse
        {
            get
            {
                return (float)(1.00d / Math.Sqrt(
                    X * X +
                    Y * Y));
            }
        }

        #endregion
        #region public methods

        public int Cross(v2i value)
        {
            int result;
            Cross(ref this, ref value, out result);
            return result;
        }
        public int Cross(int x, int y)
        {
            return Cross(new v2i(x, y));
        }
        public float Dot(v2i value)
        {
            float result;
            Dot(ref this, ref value, out result);
            return result;
        }
        public float Dot(int x, int y)
        {
            return Dot(new v2i(x, y));
        }

        public v2i Min(v2i value)
        {
            v2i result;
            Min(ref this, ref value, out result);
            return result;
        }
        public v2i Min(int x, int y)
        {
            return Min(new v2i(x, y));
        }
        public v2i Min(int value)
        {
            v2i result;
            Min(ref this, value, out result);
            return result;
        }
        public float Min()
        {
            float result;
            v2i.Min(ref this, out result);
            return result;
        }

        public v2i Max(v2i value)
        {
            v2i result;
            Max(ref this, ref value, out result);
            return result;
        }
        public v2i Max(int x, int y)
        {
            return Max(new v2i(x, y));
        }
        public v2i Max(int value)
        {
            v2i result;
            Max(ref this, value, out result);
            return result;
        }
        public float Max()
        {
            float result;
            v2i.Max(ref this, out result);
            return result;
        }

        public v2i Clamp(v2i min, v2i max)
        {
            v2i result;
            Clamp(ref this, ref min, ref max, out result);
            return result;
        }
        public v2i Clamp(int min, int max)
        {
            v2i result;
            Clamp(ref this, min, max, out result);
            return result;
        }

        public v2i Lerp(v2i value, float percent)
        {
            v2i result;
            Lerp(ref this, ref value, percent, out result);
            return result;
        }
        
        #endregion
        #region public methods (object)

        public bool Equals(v2i value)
        {
            return
                this.X == value.X &&
                this.Y == value.Y;
        }
        public override bool Equals(object value)
        {
            return (value is v2i) &&
                ((v2i)value).Equals(this);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                const int n1 = MathF.m_hash1;
                const int n2 = MathF.m_hash2;

                return (n2 * (n2 * n1 + X) + Y);
            }
        }
        public override string ToString()
        {
            return String.Format(
                "[{0}] X({1}) Y({2})",
                GetType().Name, X, Y);
        }

        #endregion
        #region public methods (IComparable)

        public int CompareTo(v2i other)
        {
            int a = Y * other.X;
            int b = X * other.Y;

            if (a > b) return +1;
            if (a < b) return -1;
            return 0;
        }

        #endregion
    }
}