﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Extensions
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains extension methods for the C# language
    /// </summary>
    /// <remarks>To be used with C#</remarks>
    public static class CSharpExtensions
    {
        // See also
        // http://www.keithschwarz.com/darts-dice-coins/
        // https://ericlippert.com/2012/02/21/generating-random-non-uniform-data/      

        #region CSharp.Array

        public static void Fill<T>(this T[] obj, T value, int index, int count)
        {
            int step = 32;
            if (count < step)
            {
                // If less than 32, just fill the array
                for (int i = 0; i < count; i++)
                    obj[index + i] = value;
            }
            else
            {
                // If greater than 32, then fill a region and repeat it
                for (int i = 0; i < step; i++)
                    obj[index + i] = value;

                // Double the region size each time
                int n = (int)Math.Log(count, 2) - 5;
                for (int i = 0; i < n; i++)
                {
                    Array.Copy(obj, index, obj, index + step, step);
                    step += step;
                }

                Array.Copy(obj, index, obj, index + step, count - step);
            }
        }
        public static void Fill<T>(this T[] obj, T value)
        {
            Fill(obj, value, 0, obj.Length);
        }

        #endregion
        #region CSharp.EventHandler

        public static void Raise<T>(this EventHandler<T> obj, object sender, T args)
            where T : EventArgs
        {
            // By using a local variable, we can apparently avoid race conditions
            // when handling asynchronous methods. This also checks for empty handlers
            // in a consistent way

            var h = obj;
            if (h != null)
                h(sender, args);
        }

        #endregion
        #region CSharp.ICollection

        public static void Add<T>(this ICollection<T> obj, IEnumerable<T> values)
        {
            if (values == null) return;
            if (values == obj)
            {
                T[] swap = new T[obj.Count];
                obj.CopyTo(swap, 0);
                Add(obj, swap);
            }
            else
            {
                foreach (var ITEM in values)
                    obj.Add(ITEM);
            }
        }
        public static void Add<T>(this ICollection<T> obj, params T[] args)
        {
            obj.Add((IEnumerable<T>)args);
        }
        public static void Remove<T>(this ICollection<T> obj, IEnumerable<T> values)
        {
            if (values == null) return;
            if (values == obj)
                obj.Clear();

            foreach (var ITEM in values)
                obj.Remove(ITEM);
        }
        public static void Remove<T>(this ICollection<T> obj, params T[] args)
        {
            obj.Remove((IEnumerable<T>)args);
        }

        public static void Add(this ICollection<v2i> obj, int x, int y)
        {
            obj.Add(new v2i(x, y));
        }
        public static void Add(this ICollection<v2f> obj, float x, float y)
        {
            obj.Add(new v2f(x, y));
        }
        public static void Add(this ICollection<v3f> obj, float x, float y, float z)
        {
            obj.Add(new v3f(x, y, z));
        }
        public static void Add(this ICollection<v4f> obj, float x, float y, float z, float w)
        {
            obj.Add(new v4f(x, y, z, w));
        }
        public static void Add<T1, T2>(this ICollection<pair<T1, T2>> obj, T1 a, T2 b)
        {
            obj.Add(new pair<T1, T2>(a, b));
        }
        public static void Add<T1, T2>(this ICollection<kvp<T1, T2>> obj, T1 a, T2 b)
            where T1 : IComparable<T1>
        {
            obj.Add(new kvp<T1, T2>(a, b));
        }

        public static bool Remove(this ICollection<v2i> obj, int x, int y)
        {
            return obj.Remove(new v2i(x, y));
        }
        public static bool Remove(this ICollection<v2f> obj, float x, float y)
        {
            return obj.Remove(new v2f(x, y));
        }
        public static bool Remove(this ICollection<v3f> obj, float x, float y, float z)
        {
            return obj.Remove(new v3f(x, y, z));
        }
        public static bool Remove(this ICollection<v4f> obj, float x, float y, float z, float w)
        {
            return obj.Remove(new v4f(x, y, z, w));
        }
        public static bool Remove<T1, T2>(this ICollection<pair<T1, T2>> obj, T1 a, T2 b)
        {
            return obj.Remove(new pair<T1, T2>(a, b));
        }
        public static bool Remove<T1, T2>(this ICollection<kvp<T1, T2>> obj, T1 a, T2 b)
            where T1 : IComparable<T1>
        {
            return obj.Remove(new kvp<T1, T2>(a, b));
        }

        public static T Peek<T>(this ICollection<T> obj)
        {
            return System.Linq.Enumerable.First(obj);
        }
        public static T Pop<T>(this ICollection<T> obj)
        {
            T result = System.Linq.Enumerable.First(obj);
            obj.Remove(result);
            return result;
        }
        public static void Push<T>(this ICollection<T> obj, T value)
        {
            obj.Add(value);
        }

        public static bool IsNullOrEmpty<T>(this ICollection<T> obj)
        {
            return (obj == null || obj.Count == 0);
        }

        public static int GetNextSize<T>(this ICollection<T> obj, int count, int chunkSize)
        {
            // No arguments: Either set to 4 (if default), or double current size
            if (count < 1 && chunkSize < 1)
            {
                return (obj.Count == 0) ? 4 : obj.Count * 2;
            }

            // No chunk size (inferred usage): Grow by powers of 2
            if (chunkSize < 1)
            {
                int n = (int)Math.Log(count, 2);
                chunkSize = (int)Math.Pow(2, n + 1);

                if (chunkSize < 4)
                    return 4;
            }

            // Chunk size not exceeded: Double in size until chunk size is reached
            if (count * 2 <= chunkSize)
                return count * 2;

            // Chunk size exceeded: Find the (N+1)th chunk that will fit all items
            return (count * 2 / chunkSize + 1) * chunkSize;
        }
        public static int GetNextSize<T>(this ICollection<T> obj, int newCount)
        {
            return GetNextSize(obj, newCount, obj.Count);
        }
        public static int GetNextSize<T>(this ICollection<T> obj)
        {
            return GetNextSize(obj, obj.Count, 0);
        }

        #endregion
        #region CSharp.IDictionary

        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> obj, TKey key)
        {
            TValue result;
            obj.TryGetValue(key, out result);
            return result;
        }
        public static void Tally<T>(this IDictionary<T, int> obj, T value)
        {
            if (!obj.ContainsKey(value))
                obj[value] = 0;

            obj[value]++;
        }

        #endregion
        #region CSharp.IDisposable

        public static void Dispose<T>(this ICollection<T> obj)
            where T : IDisposable
        {
            if (obj == null)
                return;

            foreach (var ITEM in obj)
                ITEM.Dispose();

            obj.Clear();
        }
        public static void Dispose<TKey, TValue>(this IDictionary<TKey, TValue> obj)
            where TValue : IDisposable
        {
            if (obj == null)
                return;

            foreach (var ITEM in obj.Values)
                ITEM.Dispose();

            obj.Clear();
        }

        #endregion
        #region CSharp.IList

        public static void ClampIndex<T>(this IList<T> obj, ref int index, ref int count)
        {
            if (obj == null)
                return;

            if (index < 0)
                index = 0;

            if (index >= obj.Count)
                index = obj.Count - 1;

            if (count < 0)
                count = 0;

            if (index + count >= obj.Count)
                count = obj.Count - index;
        }
        public static bool ContainsIndex<T>(this IList<T> obj, int index)
        {
            return (index >= 0 && index < obj.Count);
        }

        public static int Sample<T>(this IList<T> obj, float percent, bool wrap)
        {
            if (obj.Count <= 1)
                return 0;

            if (wrap)
            {
                percent %= 1.00f;
                if (percent == Single.NaN) return 0;
                if (percent < 0.00f)
                    percent += 1.00f;
            }
            else
            {
                if (percent <= 0.00f) return 0;
                if (percent >= 1.00f) return obj.Count - 1;
            }

            return (int)(percent * obj.Count);
        }
        public static int Sample<T>(this IList<T> obj, float percent)
        {
            return Sample(obj, percent, true);
        }

        #endregion
        #region CSharp.Random

        public static float Nextf(this Random obj)
        {
            return (float)obj.NextDouble();
        }
        public static v2f Next2f(this Random obj)
        {
            return new v2f(
                (float)obj.NextDouble(),
                (float)obj.NextDouble());
        }
        public static v3f Next3f(this Random obj)
        {
            return new v3f(
                (float)obj.NextDouble(),
                (float)obj.NextDouble(),
                (float)obj.NextDouble());
        }
        public static v4f Next4f(this Random obj)
        {
            return new v4f(
                (float)obj.NextDouble(),
                (float)obj.NextDouble(),
                (float)obj.NextDouble(),
                (float)obj.NextDouble());
        }

        public static v2f NextRadius(this Random obj, float min, float max)
        {
            double v = obj.NextDouble() * (max - min) + min;

            return new v2f(
                (float)Math.Cos(v),
                (float)Math.Sin(v));
        }
        public static v2f NextRadius(this Random obj)
        {
            double v = obj.NextDouble() * Math.PI * 2.00;

            return new v2f(
                (float)Math.Cos(v),
                (float)Math.Sin(v));
        }
        public static v2f NextCircle(this Random obj)
        {
            double u = Math.Sqrt(obj.NextDouble());
            double v = obj.NextDouble() * Math.PI * 2.00;

            return new v2f(
                (float)(u * Math.Cos(v)),
                (float)(u * Math.Sin(v)));
        }

        public static int NextIndex<T>(this Random obj, IList<T> values, int index, int count)
        {
            if (values == null)
                return 0;

            return obj.Next(0, count) + index;
        }
        public static int NextIndex<T>(this Random obj, IList<T> values)
        {
            if (values == null)
                return 0;

            return obj.Next(0, values.Count);
        }
        public static int NextWeighted(this Random obj, IList<float> weights, float sum)
        {
            if (weights == null ||
                weights.Count == 1)
                return 0;

            if (sum <= 0.00f)
                return 0;

            float result = (float)(obj.NextDouble() * sum);
            for (int i = 0; i < weights.Count; i++)
            {
                if (result >= weights[i])
                    result -= weights[i];
                else
                    return i;
            }

            return 0;
        }

        public static void Shuffle<T>(this Random obj, IList<T> values, int index, int count)
        {
            // Fisher-Yates shuffle
            T swap;
            while (count > 1)
            {
                int n = obj.Next(0, count--);
                swap = values[index + count];
                values[index + count] = values[index + n];
                values[index + n] = swap;
            }
        }
        public static void Shuffle<T>(this Random obj, IList<T> values)
        {
            Shuffle(obj, values, 0, values.Count);
        }

        #endregion
        #region CSharp.String

        public static bool IsEmpty(this string obj)
        {
            return String.IsNullOrWhiteSpace(obj);
        }

        #endregion
    }
}