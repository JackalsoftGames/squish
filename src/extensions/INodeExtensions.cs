﻿/*
 * Squish Framework
 * 
 * Author: Jackalsoft Games
 * Date:   Apr 1, 2024
 * 
 * This code is released under the GPL v3.0 license.
 * All rights (c) their respective parties.
 */

namespace Squish.Extensions
{
    #region using
    using System;
    using System.Collections.Generic;
#if SFML
    using SFML;
    using SFML.Audio;
    using SFML.Graphics;
    using SFML.System;
    using SFML.Window;
#endif
    using Squish;
    using Squish.Collections;
    using Squish.Extensions;
    using Squish.Utilities;
    #endregion

    /// <summary>
    /// Contains extension methods for the Squish.INode interface
    /// </summary>
    /// <remarks>To be used with Squish.INode</remarks>
    public static class INodeExtensions
    {
        #region INode.GetRoot

        public static T GetRoot<T>(this INode<T> obj)
            where T : INode<T>
        {
            if (obj == null)
                return default(T);

            while (obj.Parent != null)
                obj = obj.Parent;

            return (T)obj;
        }

        #endregion
        #region INode.IsRoot

        public static bool IsRoot<T>(this INode<T> obj)
            where T : INode<T>
        {
            if (obj == null)
                return false;

            return (obj.Parent == null);
        }

        #endregion

        #region INode.IsRootOf

        public static bool IsRootOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            return obj.Equals(node.GetRoot());
        }

        #endregion
        #region INode.IsParentOf

        public static bool IsParentOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            return obj.Equals(node.Parent);
        }

        #endregion
        #region INode.IsChildOf

        public static bool IsChildOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            return node.Equals(obj.Parent);
        }

        #endregion

        #region INode.IsSiblingOf

        public static bool IsSiblingOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            return
                (obj.Parent != null) &&
                (obj.Parent.Equals(node.Parent));
        }

        #endregion
        #region INode.IsRelativeOf

        public static bool IsRelativeOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            return obj.GetRoot().Equals(node.GetRoot());
        }

        #endregion
        #region INode.IsAncestorOf

        public static bool IsAncestorOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            for (node = node.Parent; node != null; node = node.Parent)
            {
                if (node.Equals(obj))
                    return true;
            }

            return false;
        }

        #endregion
        #region INode.IsDescendantOf

        public static bool IsDescendantOf<T>(this INode<T> obj, INode<T> node)
            where T : INode<T>
        {
            if (obj == null || node == null)
                return false;

            for (obj = obj.Parent; obj != null; obj = obj.Parent)
            {
                if (obj.Equals(node))
                    return true;
            }

            return false;
        }

        #endregion
    }
}