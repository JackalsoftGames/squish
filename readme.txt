﻿The Squish Framework is a collection of general-purpose code written in C# on top of the SFML framework.
It is designed to be both high performance and general enough that it can be used in multiple applications.

My motivation is that I dislike the idea of making local edits to 3rd party libraries, in addition to being
very poor at mangaging environmental code and libraries. This project therefore exists as a companion library
to extend or alter some of SFML's features.

For example, I might dislike naming conventions, or feel that SFML's immediate drawing modes are inefficient.
One particular bug I found in the C# wrapper was that SFML.Vector3f had subtraction errors! I am unsure when or
if this was ever corrected. Not to say my code is perfect, but I have my own style and conventions that I prefer
to work with.

This code more or less exists for myself. I post it here as a public backup, both for archival purposes, and
perhaps for someone to learn from what I am doing. There are definitely better engines and projects out there. I
have found that I often restart projects, so this lets me build up a common codebase for the next attempt.

- Dan